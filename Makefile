

all:
	git pull
	sudo /etc/init.d/apache2 reload

install:
	ln -fs /var/www/dev_vortprovizo/config/apache2/dev_vortprovizo.conf /etc/apache2/sites-available/
	ln -fs /var/www/staging_vortprovizo/config/apache2/stage_vortprovizo.conf /etc/apache2/sites-available/
	ln -fs /var/www/vortprovizo/config/apache2/vortprovizo.conf /etc/apache2/sites-available/
	/usr/sbin/a2ensite stage_vortprovizo
	/usr/sbin/a2ensite dev_vortprovizo
	/usr/sbin/a2ensite vortprovizo
