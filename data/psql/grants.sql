CREATE ROLE "www-data";
ALTER ROLE "www-data" WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;

GRANT SELECT,INSERT ON TABLE public.audio TO "www-data";
GRANT USAGE,UPDATE ON SEQUENCE public.audio_id_seq TO "www-data";
GRANT SELECT,INSERT ON TABLE public.images TO "www-data";
GRANT USAGE,UPDATE ON SEQUENCE public.images_id_seq TO "www-data";
GRANT SELECT,INSERT ON TABLE public.phrase_audio TO "www-data";
GRANT SELECT,INSERT ON TABLE public.phrase_image TO "www-data";
GRANT SELECT,INSERT ON TABLE public.phrases TO "www-data";
GRANT USAGE,UPDATE ON SEQUENCE public.phrases_id_seq TO "www-data";

GRANT SELECT,INSERT,UPDATE ON TABLE public.sessions TO "www-data";
GRANT USAGE,UPDATE ON SEQUENCE public.sessions_id_seq TO "www-data";
GRANT SELECT,INSERT ON TABLE public.users TO "www-data";

GRANT SELECT,INSERT,UPDATE ON TABLE public.session_csrf TO "www-data";
GRANT USAGE,UPDATE ON SEQUENCE public.session_csrf_id_seq TO "www-data";
