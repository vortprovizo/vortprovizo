
create table wikientity (
    entity_id integer not null,
    phrase_id integer not null REFERENCES phrases(id),
    description varchar(120),
    PRIMARY KEY(entity_id, phrase_id)
);

create table wikirelation (
    parent_id integer not null,
    child_id integer not null,
    PRIMARY KEY(parent_id, child_id)
);



