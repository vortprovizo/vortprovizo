
function collector_ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fn);
    } else {
        document.attachEvent('onreadystatechange', function() {
            if (document.readyState != 'loading')
                fn();
        });
    }
}

collector_ready(function() {
    var error_timeout;
    var xhttp = new XMLHttpRequest();
    var submit = function(type, data) {
        if (error_timeout) {
            console.log("XOB " + error_timeout);
            window.clearTimeout(error_timeout);
            error_timeout = null;
            //	  var messages = document.getElementsByClassName('netError');
            //	  for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
            //	    messages[i].classList.add("display-none");
            //	  }
        }
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
                try {
                    if (this.status == 200) {
                        console.log(this.responseText);
                        // clear the form element
                        var file_form = document.getElementById("file_form");
                        file_form.add_file.value = "";
                        var data = JSON.parse(this.responseText);
                        console.log(data);
                        if (type == 'delete') {
                           window.history_back();
                        }
                    } else {
                        throw ("bad");
                    }
                } catch (err) {
                    console.log("err: " + err);
                    var messages = document.getElementsByClassName('netError');
                    error_timeout = setTimeout(function() {
                        error_timeout = null;
                        var messages = document.getElementsByClassName('netError');
                        for (var i = 0, len = messages.length | 0; i < len; i = i + 1) {
                            messages[i].classList.add("hidden");
                        }
                    }, 4000);  // Display error message for 4 seconds
                    for (var i = 0, len = messages.length | 0; i < len; i = i + 1) {
                        messages[i].classList.remove("hidden");
                    }
                }
            }
        };

        switch(type) {
        case 'add':
            xhttp.open("POST", '/api/v1/file', true);
            xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
            xhttp.setRequestHeader('Accept', 'application/json');
            xhttp.send(data);
            break;
        case 'delete':
            xhttp.open("DELETE", '/api/v1/phrase', true);
            xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
            xhttp.setRequestHeader('Accept', 'application/json');
            xhttp.send(data);
            break;
        default: 
            console.log("Unknown type: " + type);
        }
    };

    var file_form = document.getElementById("file_form");
    file_form.addEventListener("submit",
        function(e) {
            e.preventDefault();
console.log('ASDF: %o', e);
            if (e.submitter.id == 'add') {
                let data = '{ "phrase_id": ' + this.phrase_id.value + ', "file": "' + this.add_file.value + '" }';
                submit(e.submitter.id, data);
            } else if (e.submitter.id == 'delete') {
                let data = '{ "phrase_id": ' + this.phrase_id.value + ' }';
                submit(e.submitter.id, data);
            } else {}
            return false;
        }
    );
});
