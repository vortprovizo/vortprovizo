
ready(function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      try {
        if (this.status == 200) {
          var data = JSON.parse(this.responseText);
	  var phrase = document.getElementsByClassName('phrase');
	  var question = document.getElementById("question_id");
	  var next_question = document.getElementById("next_question_id");
	  var player = document.getElementById("player");
	  var images = document.querySelectorAll('img');
	  var button = document.getElementsByClassName('audio_button');
	  var mute   = document.getElementById('mute_audio');

	  question.value = data['id'];
	  next_question.value = data['next_id'];

          for (let v of phrase) {
             v.innerHTML = data["phrase"];
          }

          count = 0;
	  x = function() {
	    if (count == 4) {
	      player.load();
	    }
	  }

          for (i = 0; i < 4; i++) {
            var img = images[i];
	    img.onload = x;
            img.setAttribute("src", data["images"][i]);
          }
	  var audio = player.getElementsByTagName('source');
          if (data["audio"]) {
            audio[0].setAttribute("src", data["audio"]);
            player.load();
	    for (let item of button) {
	      item.disabled = false;
	    }
            if (!mute.checked) {
              let startPlayPromise = player.play();
            }
          } else {
            audio[0].removeAttribute("src");
	    for (let item of button) {
	      item.disabled = true;
	    }
	  }

          for (var i = 0; i < data["next_images"].length; i++) {
            var myImage = new Image(100, 200);
            myImage.src = data["next_images"][i];
            //console.log("PreloadImage: " + data["next_images"][i] + ' :: ' + myImage);
          }
          //console.log(Object.keys(data));
          if ( data.next_audio ) {
            //console.log("AUDIO %s", data.next_audio);
            myAudio = new Audio();
            myAudio.preload = "auto";
            myAudio.setAttribute("src", data.next_audio);
            myAudio.load();
          }
        } else {
           throw("bad: ")
        }
      }
      catch(err) {
        console.log("DIE %o", err);
        var messages = document.getElementsByClassName('netError');
        error_timeout = setTimeout(function() {
          error_timeout = null;
          var messages = document.getElementsByClassName('netError');
          for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
            messages[i].classList.add("display-none");
          }
        }, 3000);
        for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
          messages[i].classList.remove("display-none");
        }
      }
    }
  };

  document.getElementById("answers_form").addEventListener("submit",
    function(e) {
      e.preventDefault();
      var answer = document.activeElement.value;
      if (answer) {
	var data = '{ "question": "' + this.question_id.value + '",' +
	             '"answer": "' + answer + '",' +
	             '"next": "' +  this.next_question_id.value + '"' +
	          ' }';
	xhttp.open("POST", '/api/v1/demo', true);
	xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
	xhttp.setRequestHeader('Accept', 'application/json');
	xhttp.send(data);
      }
      return false;
    }
  );
});
