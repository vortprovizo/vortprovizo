
function collector_ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', fn);
  } else {
    document.attachEvent('onreadystatechange', function() {
      if (document.readyState != 'loading')
        fn();
    });
  }
}

collector_ready(function() {
console.log('run')
  var error_timeout;
  var xhttp = new XMLHttpRequest();
  var submit = function(data) {
    if (error_timeout) {
      console.log("XOB " + error_timeout);
      window.clearTimeout(error_timeout);
      error_timeout = null;
  //	  var messages = document.getElementsByClassName('netError');
  //	  for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
  //	    messages[i].classList.add("display-none");
  //	  }
    }
    message = function(element) {
      var messages = document.getElementsByClassName(element);
      error_timeout = setTimeout(function() {
        error_timeout = null;
        for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
          messages[i].classList.add("display-none");
          messages[i].classList.add("hide");
        }
      }, 3000);
      for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
        messages[i].classList.remove("display-none");
        messages[i].classList.remove("hide");
      }
    };
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
	try {
	  if (this.status == 409) {
            message('dupError');
            var elements = document.getElementsByName("phrase");
            console.dir(elements[0].value = "");
	  } else if (this.status == 200) {
	    var data = JSON.parse(this.responseText);
	    console.log(data);
	    console.log(data.phrase);
	    var li = document.createElement("li");
	    var tag = document.createElement("a");
	    tag.href = "/admin/phrase/" + data.language + "/" + data.phrase;
	    li.appendChild(tag);
	    li.appendChild( document.createTextNode(" 0 0") );
            var text = document.createTextNode(data.phrase);
	    tag.appendChild(text);
            var element = document.getElementById("newPhrases");
	    element.appendChild(li);
	    element.prepend(li);
	  } else {
	    console.log("bad: " + this.status);
	    throw("bad");
	  }
	} catch(err) {
	  message('netError');
	}
      }
    };

    xhttp.open("POST", '/api/v1/phrases', true);
    xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhttp.setRequestHeader('Accept', 'application/json');
    xhttp.send(data);
  };

  var file_form = document.getElementById("add_phrase_form");

  file_form.addEventListener("submit", 
    function(e) {
      e.preventDefault();
      if ( this.phrase.value.length > 1) {
      var data = '{ "phrase": "' + this.phrase.value + '", "language": "' + this.language.value + '"  }';
        submit(data);
      }

      return false;
    }
  );
});

