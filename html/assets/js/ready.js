
function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', fn);
  } else {
    document.attachEvent('onreadystatechange', function() {
      if (document.readyState != 'loading')
        fn();
    });
  }
}

ready(function() {
  // When the user scrolls the page, execute myFunction
  window.onscroll = function() {myFunction();};

  function myFunction() {
    if (window.pageYOffset > offset && (window.pageYOffset + sticky_height) < offset_b) {
      sticky.classList.remove("display-none");
    } else {
      sticky.classList.add("display-none");
    }
  }

  // Get the header
  var header = document.getElementById("stickyHeader");
  if (header) {
    var footer = document.getElementById("stickyFooter");

    // Get the offset position of the navbar
    var offset = header.offsetTop;
    var offset_b = footer.offsetTop;

    var sticky = document.getElementById("stickyStuck");
    var sticky_height = header.clientHeight;
    
    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  }
  if (typeof preload_image !== 'undefined') {
     let arrayLength = preload_image.length;
     for (var i = 0; i < arrayLength; i++) {
       var myImage = new Image(100, 200);
       myImage.src = preload_image[i];
//       console.log("PreloadImage: " + preload_image[i] + ' :: ' + myImage);
     }
  }
  if (typeof preload_audio !== 'undefined') {
     let arrayLength = preload_audio.length;
     for (let i = 0; i < arrayLength; i++) {
       myAudio = new Audio();
       myAudio.preload = "auto";
       myAudio.setAttribute("src", preload_image[i]);
       myAudio.load();
       myAudio.addEventListener('canplaythrough', function() { false; }, false);
       preload_audio[0] = myAudio;
     }
  }
  /* add events */
  var cookieConsent = document.getElementById("cookieConsent");
  if (cookieConsent) {
    if (getCookie('have_gdpr')) {
	cookieConsent.classList.add('display-none');
    } else {
      var gdpr = document.getElementById("gdpr-consent");
      if (gdpr) {
	gdpr.onclick = function() {
	  var x = document.getElementById("cookieConsent");
	  cookieConsent.classList.add('display-none');
	  setCookie('have_gdpr', true, 90);
	};
      }
    }
  } else {
    console.log('add cookieConsent');
  }
});

