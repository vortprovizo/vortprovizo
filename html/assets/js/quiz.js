
ready(function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      try {
        if (this.status == 200) {
//          console.log(this.responseText);
          var data = JSON.parse(this.responseText);
	  var phrase = document.getElementsByClassName('phrase');
	  var question = document.getElementById("question_id");
	  var player = document.getElementById("player");
	  var images = document.querySelectorAll('img');
	  var button = document.getElementsByClassName('audio_button');
	  var mute   = document.getElementsByIdName('mute_audio');


	  question.value = data['id'];

          for (let v of phrase) {
             v.innerHTML = data["phrase"];
          }

          count = 0;
	  x = function() {
	    count++;
//	    console.log("asdf: ", count);
            console.log(mute);

	    if (count == 4) {
              console.log(mute);
	      player.load();
	    }
	  }

          for (i = 0; i < 4; i++) {
            var img = images[i];
	    img.onload = x;
            img.setAttribute("src", data["images"][i]);
          }
	  var audio = player.getElementsByTagName('SOURCE');
          if (data["audio"]) {
            audio[0].setAttribute("src", data["audio"]);
	    for (let item of button) {
	      item.disabled = false;
	    }
	    // if new
//            player.load();
          } else {
            audio[0].removeAttribute("src");
	    for (let item of button) {
	      item.disabled = true;
	    }
	  }
        } else {
           thow("bad")
        }
      }
      catch(err) {
        var messages = document.getElementsByClassName('netError');
	console.log("BILL " + err);
        error_timeout = setTimeout(function() {
          error_timeout = null;
          var messages = document.getElementsByClassName('netError');
          for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
            messages[i].classList.add("display-none");
          }
        }, 3000);
        for (var i = 0, len=messages.length | 0; i < len; i = i + 1) {
          messages[i].classList.remove("display-none");
        }
      }
    }
  };

  document.getElementById("answers_form").addEventListener("submit",
    function(e) {
      e.preventDefault();
      var answer = document.activeElement.value;
      if (answer) {
	var data = '{ "question": "' + this.question.value + '", "answer": "' + answer + '" }';
	xhttp.open("POST", '/api/v1/quiz', true);
	xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
	xhttp.setRequestHeader('Accept', 'application/json');
	xhttp.send(data);
      }
      return false;
    }
  );
});
