
document.onreadystatechange = () => {
  if (document.readyState === 'complete') {
     var sign_in_tab = document.getElementById("sign_in_tab")
     var sign_up_tab = document.getElementById("sign_up_tab")
     var sign_in_pane = document.getElementById("sign-in-pane")
     var sign_up_pane = document.getElementById("sign-up-pane")

     var key = window.location.hash;

     console.log(key);

     if (key && key == '#sign-in') {
	 sign_in_tab.classList.add("active");
	 sign_up_tab.classList.remove("active");
	 sign_in_pane.classList.add("active");
	 sign_up_pane.classList.remove("active");
     }

     var myScript = function(name) {
         return function(event) {
	   console.log("name: " + name + " :: " + event);
	   if (name == "sign_in") {
	       sign_in_tab.classList.add("active");
	       sign_up_tab.classList.remove("active");
	       sign_in_pane.classList.add("active");
	       sign_up_pane.classList.remove("active");
	   } else {
	       sign_up_tab.classList.add("active");
	       sign_in_tab.classList.remove("active");
	       sign_up_pane.classList.add("active");
	       sign_in_pane.classList.remove("active");
	   }
	 }
     }

     sign_in_tab.addEventListener("click", myScript("sign_in"));
     sign_up_tab.addEventListener("click", myScript("sign_up"));
  }
};

function openCity(cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none"; 
  }
  document.getElementById(cityName).style.display = "block"; 
}
