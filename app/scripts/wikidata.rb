
require 'vortprovizo/pg'
require 'net/http'
require 'uri'
require 'json'

conn = Vortprovizo::Pg.connection

rs = conn.exec(<<~SQL)
  select entity_id
       , phrase_id
       , description
       , phrase
       , language_id
       , iso2
       , iso3
    from wikientity w
    join phrases p
      on p.id = w.phrase_id
   join languages l
     on l.id = language_id
SQL

rs.each do |row|
  puts row

  url = 'https://www.wikidata.org/wiki/Special:EntityData?id=Q%s&format=json' % [ row['entity_id'] ]
  uri = URI(url)

  Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
    ret = http.request_get(uri.request_uri)

    data = JSON.parse(ret.body)
    enity_id = 'Q' + row['entity_id']

    puts data['entities'][enity_id]['labels']['en']
    puts data['entities'][enity_id]['labels']['nl']

  end
end

__END__
https://www.wikidata.org/w/index.php?search=aandoen&title=Special%3ASearch&profile=advanced&fulltext=1&ns146=1&format=json

