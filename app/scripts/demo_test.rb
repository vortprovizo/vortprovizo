#!/usr/bin/env ruby

require 'vortprovizo/demo'
require 'pp'
require 'narray'
require 'histogram'
require 'histogram/array'

d = Vortprovizo::Demo.new('en')
phrases = Hash.new { |h, k| h[k] = 0 }
images  = Hash.new { |h, k| h[k] = 0 }
audio   = Hash.new { |h, k| h[k] = 0 }

5000.times.each do |i|
  p = d.get_quiz

  pp p

  exit

  phrases[p[:phrase]] += 1
  audio[p[:audio] || 'none'] += 1
  p[:images].each do |i|
    images[i] += 1
  end
end

pp x = Hash[ * phrases.values.group_by { |v| v }.flat_map { |k, v| [ k, v.size ] } ]

pp x.keys.sort { |a| a.to_i <=> a.to_i }

a = Array[]

x.keys.sort.each do |n|
  pp [ n, x[n] ]
  a[n] = x[n]
end

pp a

# Hash[*data.group_by{ |v| v }.flat_map{ |k, v| {k, v.size} }]

# (bins, freqs) = phrases.values.to_a.histogram
# pp bins, freqs

# (bins, freqs) = images.values.to_a.histogram
# pp bins, freqs
# pp images.values.size
# pp images
# pp phrases.values.to_a.tally

__END__

{:phrase=>"vier",
 :audio=>"https://upload.wikimedia.org/wikipedia/commons/b/b7/Nl-vier.ogg",
 :audio_type=>"application/ogg",
 :images=>
  ["https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Playing_Card,_Four_of_Clubs,_late_19th_century_(CH_18405315).jpg/190px-Playing_Card,_Four_of_Clubs,_late_19th_century_(CH_18405315).jpg",
   "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Gluehlampe_01_KMJ.jpg/190px-Gluehlampe_01_KMJ.jpg",
   "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Fireplace_poker.jpg/190px-Fireplace_poker.jpg",
   "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/IceAgeEarth.jpg/190px-IceAgeEarth.jpg"],
 :id=>602}

