# frozen_string_literal: true

require 'minitest/autorun'
require 'vortprovizo/genpage'

class Vortprovizo
  class GenpageTest < Minitest::Test
    def test_one
      [ 'demo',
        'entry_form',
        'entry_reset',
        'gdpr',
        'home',
        'index',
        'phrase',
        'phrases',
        'privacy_policy',
        'quiz',
        'terms', ].each do |f|
        gt = Vortprovizo::Genpage.new('en').genpage(f)
  #     pp gt.class.ancestors
        assert_instance_of Nokogiri::HTML5::Document, gt, 'Expect HTML5 object'
      end
    end

    def test_two
      [ 'no_file_called_this' ].each do |f|
        assert_raises(ArgumentError) do
          puts Vortprovizo::Genpage.new('en').genpage(f)
        end
      end
    end
  end
end
