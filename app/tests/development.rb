# frozen_string_literal: true

require 'http'
require 'test/unit'
require 'pp'

class TestDemo < MiniTest::Test
  def test_true
    ENV['RACK_ENV'] = 'development'

    demo = Http.new(language: 'nl')

    assert_true(demo.development?)
  end

  def test_false
    ENV['RACK_ENV'] = 'production'

    demo = Http.new(language: 'nl')

    assert_true(!demo.development?)
  end
end
