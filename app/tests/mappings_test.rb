
require 'test_helper'
require 'mappings'

class Mappings
  def self.reset
    @@mappings = [ ]
  end
end

class MappingsClassTests < Minitest::Test
  def call(env)
    return 'OK'
  end
  def test_register
    Mappings.reset()
    Mappings.register(self, [ '/bob' ])
    assert_output(/duplicate/, nil) {
      Mappings.register(self, [ '/bob' ])
    }
  end

  def test_register_id
    Mappings.reset()
    Mappings.register(self, [ '/bob/{x}' ])
    assert_output(/The package/, nil) {
      Mappings.register(self, [ '/bob/{y}' ])
    }
  end
  def test_empty
    #http = self
    map = Mappings.new(self )
    assert_match('OK', map.call({}))
  end
end

