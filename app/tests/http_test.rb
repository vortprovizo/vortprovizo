
require 'test_helper'
require 'http'

class HttpClassTests < Minitest::Test
  def test_packages
    assert_kind_of(Array, Http.packages.to_a)
  end
  def test_call
    Http.call({})
  end
end

class HttpTests < Minitest::Test
  def setup
    @self = Http.new({ })
  end

  def test_methods
    assert_equal(false, @self.development?)
  end

  def test_call
    assert_equal([ 404, {"content-type"=>"text/plain"}, [ "Not Found" ] ], @self.call())
  end
end

class HttpDevTests < Minitest::Test
  def setup
    mock_env( 'RACK_ENV' => 'development' ) do
      @self = Http.new('development')
    end
  end

  def test_methods
    assert_equal(true, @self.development?)
  end

  def test_call
    data = @self.call()
    assert_equal(404, data[0])
  end
end

