# frozen_string_literal: true

require 'test_helper'
require 'vortprovizo/utils'
#require 'testunit-test-util'
require 'test/unit'

class TestVortprovizoTools < MiniTest::Test
  def test_simple
    assert_equal([ 'en' ], Vortprovizo::Tools.parse_accept_language('en'))
  end

  def test_complex
    assert_equal(
      [ 'en-US', 'en', 'nl', 'fr', 'fy' ],
      Vortprovizo::Tools.parse_accept_language('en-US,en;q=0.9,nl;q=0.8,fr;q=0.7,fy;q=0.6')
    )
  end
end
