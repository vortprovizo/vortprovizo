# frozen_string_literal: true

require 'test_helper'
require 'vortprovizo/languages'

class TestLanguages < Minitest::Test
  def test_simple
    assert_equal('eng', Vortprovizo::Languages.get('en').iso3)
    assert_equal('cho', Vortprovizo::Languages.get('cho').iso3)
    assert_equal('nld', Vortprovizo::Languages.get('nl').iso3)
    assert_equal('nl', Vortprovizo::Languages.get('nld').iso2)
  end

  def test_name
    assert_equal('English', Vortprovizo::Languages.get('en').english)
    assert_equal('note', Vortprovizo::Languages.get('en').native)
  end
end

