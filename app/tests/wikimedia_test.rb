
require 'test_helper'
require 'minitest/mock'
require 'wikimedia'

class TestWikimediaFilePath < Minitest::Test
  def test_one
    assert_equal('https://commons.wikimedia.org/wiki/File:Dog.png',
                 Wikimedia.file_path('Dog.png'))
  end

  def test_thumbnail
    assert_equal('https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Dog.ogg/190px-Dog.ogg',
                 Wikimedia.thumbnail('Dog.ogg'))
    assert_equal('https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Dog.svg/190px-Dog.svg.png',
                 Wikimedia.thumbnail('Dog.svg'))
    assert_equal('https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Dog.tif/190px-Dog.tif.jpg',
                 Wikimedia.thumbnail('Dog.tif'))
  end

  def test_audio
    assert_equal('https://upload.wikimedia.org/wikipedia/commons/e/e8/Dog.ogg',
                 Wikimedia.audio('Dog.ogg'))
  end

  def test_uri
    assert_equal(URI('https://upload.wikimedia.org/wikipedia/commons/e/e8/Dog.ogg'),
                 Wikimedia.uri('Dog.ogg'))
  end

  def test_check_image
    request_mock = MiniTest::Mock.new
    request_mock.expect(:code, 200, [ ])
    request_mock.expect(:code, 200, [ ])
    request_mock.expect(:[], 'application/ogg', [ 'content-type' ])
    http_mock = MiniTest::Mock.new
    http_mock.expect(:request_head, request_mock, [ String ])

    Net::HTTP.stub(:start, [ ], http_mock) do
      assert_equal('asdf.png', Wikimedia.check_image('File:asdf.png'), "ogg file")
    end
  end

  def test_check_audio
    request_mock = MiniTest::Mock.new
    request_mock.expect(:code, 200, [ ])
    request_mock.expect(:code, 200, [ ])
    request_mock.expect(:[], 'application/ogg', [ 'content-type' ])
    http_mock = MiniTest::Mock.new
    http_mock.expect(:request_head, request_mock, [ String ])

    Net::HTTP.stub(:start, [ ], http_mock) do
      assert_equal('asdf.ogg', Wikimedia.check_audio('File:asdf.ogg'), "ogg file")
    end
  end

  def test_check_type

    assert_equal(:entity, Wikimedia.check_type('Q12345'))

    [ 'application/ogg', 'audio/x-wav', ].each do |type|
      request_mock = MiniTest::Mock.new
      request_mock.expect(:code, 200, [ ])
      request_mock.expect(:[], type, [ 'content-type' ])
      http_mock = MiniTest::Mock.new
      http_mock.expect(:request_head, request_mock, [ String ])

      Net::HTTP.stub(:start, [ ], http_mock) do
        assert_equal(:audio, Wikimedia.check_type('xxxxxx'))
      end
    end

    [ 'image/jpeg', 'image/svg+xml', 'image/png', 'image/tiff', 'image/gif' ].each do |type|
      request_mock = MiniTest::Mock.new
      request_mock.expect(:code, 200, [ ])
      request_mock.expect(:[], type, [ 'content-type' ])
      http_mock = MiniTest::Mock.new
      http_mock.expect(:request_head, request_mock, [ String ])

      Net::HTTP.stub(:start, [ ], http_mock) do
        assert_equal(:image, Wikimedia.check_type('xxxxxx'))
      end
    end

    begin
      request_mock = MiniTest::Mock.new
      request_mock.expect(:code, 200, [ ])
      request_mock.expect(:[], 'text/html', [ 'content-type' ])
      http_mock = MiniTest::Mock.new
      http_mock.expect(:request_head, request_mock, [ String ])

      Net::HTTP.stub(:start, [ ], http_mock) do
        raise 'not reached'
      end
      assert(false)
    rescue => e
      assert_equal("not reached", e.to_s)
      assert(true)
    end

    begin
      request_mock = MiniTest::Mock.new
      request_mock.expect(:code, 500, [ ])
      request_mock.expect(:[], 'text/html', [ 'content-type' ])
      http_mock = MiniTest::Mock.new
      http_mock.expect(:request_head, request_mock, [ String ])

      Net::HTTP.stub(:start, [ ], http_mock) do
        raise 'not reached'
      end
      assert(false)
    rescue => e
      assert(true)
    end

    begin
      request_mock = MiniTest::Mock.new
      request_mock.expect(:[], 'text/html', [ 'content-type' ])
      http_mock = MiniTest::Mock.new
      http_mock.expect(:request_head, request_mock, [ String ])

      Net::HTTP.stub(:start, lambda { |a, b, c|  }) do
        raise 'not reached'
      end
      assert(false)
    rescue => e
      assert(true)
    end
  end
end

__END__

