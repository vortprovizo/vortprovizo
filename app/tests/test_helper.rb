

require 'simplecov'
SimpleCov.start do
  add_filter "tests/"
end
puts "SimpleCov started successfully!"

require 'minitest/autorun'

def mock_env(partial_env_hash)
  old = ENV.to_hash
  ENV.update partial_env_hash
  begin
    yield
  ensure
    ENV.replace old
  end
end


