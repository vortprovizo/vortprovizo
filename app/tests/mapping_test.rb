
require 'test_helper'
require 'mappings'

class MappingClassTests < Minitest::Test
  def test_empty
    map = Mappings::Mapping.new()
    assert_nil(map.match('/bob'))
  end

  def test_bob
    map = Mappings::Mapping.new(regexp: %r{/bob})
    assert_equal('/bob', map.match('/bob').to_s)
  end

  class Test
    def call(env)

    end
  end

  def test_package
    map = Mappings::Mapping.new(regexp: %r{/bob}, package: lambda { |env| return 'OK' } )
    assert_equal('OK', map.call({}))
  end

  def test_hash
    map = Mappings::Mapping.new(route: 'a', package: 'b' )
    map_hash = map.to_h
    assert_match(String.to_s, map_hash['route'].class.to_s)
    assert_match(Regexp.to_s, map_hash['nroute'].class.to_s)
    assert_match(String.to_s, map_hash['package'].class.to_s)
  end
end

