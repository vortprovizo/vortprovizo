# frozen_string_literal: true

require 'mappings'
require 'set'

# This is the base class for all Http handlers
# such as the Web and Api Handlers
class Http
  # this contains a list of package names
  @@packages = Set.new

  # Call this static method to add a route to a Package
  def self.register_routes(*routes)
puts 'register_routes %s' % [ self ]
    @@packages.add self
    Mappings.register(self, routes)
  end

  # Return the list of _registered_ Packages
  # @todo
  def self.packages
    @@packages
  end

  # @private
  def initialize(env)
    @env = env
    @development = ENV['RACK_ENV'] ==  'development'
  end

  def development?
   @development
  end

  # Get a parameters value by name
  # @param <String> name
  def params(name)
    @params[name][0]
  end

  # This is a entry point from RakeUp
  # @param env The Environment from `rakeup`
  # @return [ int, Hash, Array ]
  def self.call(env)
    puts "calling %s" % self
    this = new(env)

    ret = this.call()

    ret
  end

  #  Not found if we reach here
  def call(_env = nil)
    if development?
      [ 404, { 'content-type' => 'text/plain' }, [
        Mappings.mappings.map { |x| '%-38s %s' % [ x.route.to_s, x.package ] }.join("\n")
      ] ]
    else
      [ 404, { 'content-type' => 'text/plain' }, [ 'Not Found' ] ]
    end
  end

  # is this used?
#  def inspect
#    puts self
#    @env
#  end
end
