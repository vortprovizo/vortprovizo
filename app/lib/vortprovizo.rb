# frozen_string_literal: true

require 'json'
require 'net/https'
require 'http-cookie'
require 'signet/oauth_2/client'
require 'cgi'
require 'jwt'
require 'vortprovizo/error'
require 'http'

# The Vortprovizo class
class Vortprovizo
  # The Vortprovizo::Http class
  class Http < Http
#    include Vortprovizo::Error
    def info(x)
      # puts caller(0)
      puts self
      if (l = @env['rack.logger'])
        l.info(x)
      end
    end
    
    # @return Session
    # The global session data
    def session()
      put "SESSION: %s" % @session
      @session
    end

    # Return a 404 error
    class NotFoundError < StandardError
    end
  end
end

__END__
