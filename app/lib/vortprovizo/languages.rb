# frozen_string_literal: true

require 'vortprovizo/db/language'

class Vortprovizo
  # The language class which holds information of a give language
  class Language
    attr_reader :name
    attr_reader :note
    def initialize(iso2, iso3, name, id)
      @iso2 = iso2
      @iso3 = iso3
      @name = name
      @id = id
      @note = 'note'
    end
    # Get the `id` of the language. 
    # @return [Integer]
    # @see the id column of the languages table in the vortprovizo database.
    def id
      return @id
    end
    # Get the `iso3` value for the language. 
    # @see the ISO_639-2 codes: https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
    # @return [String]
    def iso3
      return @iso3
    end
    # Get the `iso2` value for the language if it exists. 
    # @see the ISO_639-1 standard https://en.wikipedia.org/wiki/ISO_639-1
    # @return [String, nil]
    def iso2
      return @iso2
    end
    # Get the `english` word that describes the language
    # @return [String, nil]
    def english
      return @name
    end
    # Get the word that describes the language in the native tounge
    # @return [String, nil]
    def native
      return @note
    end
  end

  # A singleton containing all know languages
  class Languages
    @languages = nil
    # initialize the singleton
    def self.init
      @languages = Hash.new
      Vortprovizo::Db::Language.get_all.each do |l|
        ll = Language.new( l[:iso2], l[:iso3], l[:note], l[:id] )
        @languages[ll.id] = ll
        @languages[ll.iso2] = ll
        @languages[ll.iso3] = ll
      end
    end

    # get a [Language] object 
    # @return [Language]
    # @param lang [String] iso2 or iso3 description
    # @raise [{ArgumentError] unknown language _language_
    # 
    # @example Request the *English*  Language
    #   Vortprovizo::Languages.get('en')
    # @example Request the *Choctaw*  Language
    #   Languages.get('cho')
    def self.get(lang)
      self.init unless @languages
      ret = @languages[lang]

      raise ArgumentError.new('Unknown language "%s"' % [ lang ]) if ret.nil?

      return ret
    end
  end
end

