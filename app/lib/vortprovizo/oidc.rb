# frozen_string_literal: true

# https://accounts.google.com/.well-known/openid-configuration
require 'oauth2'
require 'net/http'
require 'json'
require 'yaml'

class Vortprovizo
  # [OpenID Connect](https://en.wikipedia.org/wiki/OpenID#OpenID_Connect_(OIDC%29)
  class Oauth2
    attr_reader :rsa_private_key
    attr_reader :userinfo_endpoint

    def initialize(client_secret:, client_id:, site:, authorization_uri:, token_uri: nil,  redirect_uri: nil,
                   rsa_private_key: nil,
                   userinfo_endpoint: nil,
                   scope: nil )
      @client_secret = client_secret
      @client_id = client_id
      @site = site
      @authorization_uri = authorization_uri
      @token_url = token_uri
      @code_verifier = SecureRandom.urlsafe_base64(32)
      @rsa_private_key = rsa_private_key
      @redirect_uri = redirect_uri
      @userinfo_endpoint = userinfo_endpoint
      @scope = scope
    end

    # get the location
    def get_location
      location
    end

    # get the client
    def client
      client = OAuth2::Client.new(
        @client_id,
        @client_secret, 
        site: @site,
        authorize_url: @authorization_uri,
        token_url: @token_url,
        redirect_uri: @redirect_uri,
      )
      client
    end

    # get the code_verifier
    def code_verifier
      @code_verifier
    end
    
    # get the location
    def location
      code_challenge = Base64.urlsafe_encode64(Digest::SHA256.digest(@code_verifier)).chomp('=')
      auth_url = client.auth_code.authorize_url(
        code_challenge: code_challenge,
        code_challenge_method: 'S256',
        state: 'w12345',
        scope: @scope,
      )

      return auth_url
    end
  end

  # OIDC Class for logging in
  class OIDC
    @@list = Hash.new

    # @private
    def self.x
      @@list
    end

    # @private
    def self.client(name)
      @@list[name].client
    end

    # @private
    def self.oauth(name)
      @@list[name]
    end

    # @private
    def self.get(name)
      @@list[name]
    end

    # @private
    def self.build

      config = YAML.load_file('.config.yaml')
      oauth2 = config['oauth2']

      oauth2.each do |k, v|
        setup = v.transform_keys(&:to_sym)
        type = setup.delete(:type)

        case type.to_sym
        when :oath2
          @@list[k.to_sym] = Oauth2.new(**setup)
        else
          raise "Unknown type %s" % type.class
        end
      end
    end

    self.build()

    # FIXME
    def self.oauth2(state: nil, code: nil)
      begin
        case state
        when 'gitlab'
          client = OIDC.gitlab_client
        when 'google'
          client = OIDC.google_client
        else
          raise 'bad state'
        end
        ix = 'ok'
        jwt = 'ok'
        ui = 'ok'
        email = nil
        client.code = code
        data =  client.fetch_access_token
        jwt = JWT.decode data['id_token'], nil, false if data

        File.open('/tmp/jwt') { |f| f.write jwt.to_json }

        case state
        when 'google'
          email = jwt[0]['email']
          email_verified = jwt[0]['email_verified']
        when 'gitlab'
          luri = URI.parse('https://gitlab.com/oauth/userinfo')
          ui = OIDC.get_userinfo(luri, data['access_token'])
          email = ui['email']
          email_verified = ui['email_verified']
        else
          raise 'bad state %s' % [ state ]
        end

        if email
          # FIXME
          return [ 303, {'content-type' => 'text/html', 'location' => '/index3.html' }, [ '<a href="%s">You are logged in as (%s)</a>' % [ ix, email ] ] ]
        end
        return [ 401, {'content-type' => 'text/html', 'location' => '/index2.html' }, [ '<a href="%s">You are NOT logged in</a>' % [ ix ] ] ]
      rescue => e
        return [ 400, {'content-type' => 'application/json'}, [ {x: e, jwt: jwt}.to_json ] ]
      end

      return [ 200, {'content-type' => 'application/json'}, [ { ui: ui, jwt: jwt, uri: uri, token: client.code, x: x,  state: state,  code: code, data: data, email: email, email_verified: email_verified }.to_json ] ]
    end

# curl -v --header 'Authorization: Bearer e52ff22f690af8edb3761b629059bfc3a4086054c75534ad8d8751d78d86d04c' https://gitlab.com/oauth/userinfo
    # @private
    def self.get_userinfo(uri, auth)
#      uri = URI.parse('https://openidconnect.googleapis.com/v1/userinfo')
      STDERR.puts "Bearer %s [ %s ]" % [ auth, uri.to_s ]
      res =
      Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
        req = Net::HTTP::Get.new(uri.request_uri)
        req['Authorization'] = "Bearer %s" % [ auth ]
        res = http.request(req)
        JSON.parse(res.body)
      end
    end
  end
end



