# frozen_string_literal: true

require 'digest'
require 'nokogumbo'

require 'vortprovizo/pg'
require 'wikimedia'

class Vortprovizo
  # A quize for logged in users
  class Quiz
    def initialize(language = 'en')
      @lang = language
    end

    # Return the data for a quiz
    def get_quiz()

	connection = Vortprovizo::Pg.connection

	rd = connection.exec(<<SQL, [ language_id ])
select p.id
     , p.phrase
  from phrases p
 inner join phrase_image pi
    on p.id = phrase_id
 where language_id = $1
 group by p.id, p.phrase
 order by random()
 limit 1
SQL

      phrase = rd[0]['phrase']
      phrase_id = rd[0]['id']

      rd = connection.exec(<<SQL, [ phrase_id ])
SELECT a.id, file
  FROM phrase_audio pa
  JOIN audio a
    ON pa.audio_id = a.id
 WHERE pa.phrase_id = $1
 ORDER by random()
 LIMIT 1
SQL
      if rd.cmd_tuples > 0
	audio = rd[0]['file']
	audio_path = Wikimedia.audio(audio)
      end

      rd = connection.exec(<<SQL, [ phrase_id ])
SELECT i.id, file
  FROM phrase_image pi
  JOIN images i
    ON pi.image_id = i.id
 WHERE pi.phrase_id = $1
 ORDER by random()
 LIMIT 1
SQL

      file = Wikimedia.thumbnail(rd[0]['file'])

      rd = connection.exec(<<SQL, [ ])
SELECT id, file
FROM  (
    SELECT DISTINCT 1 + trunc(random() * 9442)::integer AS id
    FROM   generate_series(1, 1100) g
    ) r
JOIN   images USING (id)
LIMIT  3
SQL
      files = rd.map { |r| Wikimedia.thumbnail(r['file']) }

      files.push file

      data = {
	phrase: phrase,
	audio: audio_path,
	audio_type: 'application/ogg',
	images: files.shuffle,
	id: 602
      }
    end
  end
end

__END__

