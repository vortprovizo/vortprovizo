# frozen_string_literal: true

require 'digest'
require 'nokogumbo'

class Vortprovizo
  # Check Images, etc by native speaker
  class Native

    def initialize
    end

    # not used
    def self.genpage(quiz: nil, file:  nil)
      date = nil
      begin
	t = Time.now
	file = File.open('../html/%s/native.html' % [ 'en' ])
	doc = Nokogiri::HTML5(file)
	doc.css('.phrase').each do |p|
	  p.inner_html = quiz['phrase']
	end
	data = doc.to_xml
      rescue => e
	data = e
      end
    end
  end
end

__END__

con = PG.connect :dbname => 'vortprovizo', :user => 'www-data'
con.exec('SET client_encoding = UTF8;', [  ])

def audio_file(file_name)
  # https://upload.wikimedia.org/wikipedia/commons/transcoded/1/1e/En-uk-a_cat.ogg/En-uk-a_cat.ogg.mp3
    file = file_name.gsub(' ', '_')
    file[0] = file[0].upcase
    x = Digest::MD5.hexdigest file
    return "https://upload.wikimedia.org/wikipedia/commons/#{x[0]}/#{x[0..1]}/#{file}"
end

def thumbnail(file_name)
  file = file_name.dup 
  file = file.gsub(' ', '_')
  file[0] = file[0].upcase
  x = Digest::MD5.hexdigest file
  ret = "https://upload.wikimedia.org/wikipedia/commons/thumb/#{x[0]}/#{x[0..1]}/#{file}/120px-#{file}"
  if file.match /.svg$/
     ret += '.png'
  elsif file.match /.tif$/
     ret += '.jpg'
  end
  return ret
end

all_phrases = Hash.new
phrase_array = Array.new
audio_array = Array.new
image_array = Array.new

rs = con.exec(<<SQL)
select p.id
     , phrase
     , audio_count
     , image_count
  from phrases p
  join (select phrase_id, count(*) as audio_count
          from phrase_audio group by phrase_id) pa
    on (p.id = pa.phrase_id)
  join (select phrase_id, count(*) as image_count
          from phrase_image group by phrase_id) pi
    on (p.id = pi.phrase_id)
 where iso3 = 'eng'
SQL

rs.each do |row|
    all_phrases[row['phrase']] = {
        id: row['id'],
    }
    phrase_array.push row['phrase']
end

phrase = phrase_array[Random.rand(phrase_array.size)]

rs = con.exec(<<SQL, [ all_phrases[phrase][:id] ])
select *
  from phrase_audio pa
  join audio a
    on pa.audio_id = a.id
 where phrase_id = $1
SQL

audio_array.push rs.to_a[Random.rand(rs.cmd_tuples)]['file']
rs.each do |row|
#    audio_array.push row['file']
end

rs = con.exec(<<SQL, [ all_phrases[phrase][:id] ])
select *
  from phrase_image pa
  join images i
    on pa.image_id = i.id
 where phrase_id = $1
SQL

image_array.push thumbnail(rs.to_a[Random.rand(rs.cmd_tuples)]['file'])
#rs.each do |row|
#    image_array.push row['image_id']
#end

rs = con.exec(<<SQL, [ 10000 ])
SELECT id, file
FROM  (
    SELECT DISTINCT 1 + trunc(random() * $1)::integer AS id
    FROM   generate_series(1, 1100) g
    ) r
JOIN   images USING (id)
LIMIT  3
SQL

rs.each do |row|
    image_array.push thumbnail(row['file'])
end

data = {
   "phrase" => phrase,
   "audio" => audio_file(audio_array[0]),
   "images" => image_array.shuffle,
   "id" => "deadbeef",
   "lang" => "en",
   "timing" => Time.now - t,
}

puts data.to_json
