# frozen_string_literal: true

require 'digest'
require 'base64'

require 'vortprovizo/languages'
require 'vortprovizo/pg'
require 'wikimedia'

class Vortprovizo
  # A card with a phrase, 4 images, and possibly an audio file
  class Card
    def initialize(id: nil, files: nil, images: [ ], audio_path: nil, language: nil, phrase: nil)
      @files = files
      @id = id
      @images = images
      @audio_path = audio_path
      @language = language
      @phrase = phrase
    end
    # Get the unique id for this card
    # @return [String]
    def id()
      keys = [ @phrase_id, @audio_id || 0 ]

      @images.each do |p|
        x = p[:id] << 1
        if p[:correct]
          x += 1
        end
        keys.push x
      end
      byte_string = keys.pack('L*')
      ret = Base64.strict_encode64(byte_string)
      return ret
    end

    # @!visibility private
    def get_phrase
      connection = Vortprovizo::Pg.connection

      rd = connection.exec_params(<<~SQL, [ @phrase_id ])
        select p.id
             , p.phrase
             , p.language_id
          from phrases p
         where p.id = $1
      SQL
      @phrase = rd[0]['phrase']
      @lang_id = rd[0]['language_id']

      self
    end

    # A string containing the phrase
    # @return [String]
    def phrase
      get_phrase unless @phrase
      @phrase
    end

    # @!visibility private
    def get_audio_path
      connection = Vortprovizo::Pg.connection

      rd = connection.exec_params(<<~SQL, [ @audio_id ])
        SELECT id, file
          FROM audio
         where id = $1
      SQL

      if rd.ntuples > 0
        @audio = rd[0]['file']
        @audio_path = Wikimedia.audio(@audio)
      end

      self
    end

    # The audion file
    # @return [nil|String]
    def audio_path
      get_audio_path unless @audio_path
      @audio_path
    end

    # @!visibility private
    def get_images
      connection = Vortprovizo::Pg.connection

      temp = {}
      @images.each do |i|
        temp[i[:id].to_s.to_sym] = i
      end

      sql = <<~SQL % [ @images.each_index.map { |i| '$%s' % [ i + 1 ] }.join(',') ]
        select id, file
          from images
         where id in ( %s )
      SQL
      rd = connection.exec_params(sql, @images.map { |i| i[:id] })

      rd.each do |row|
        temp[row['id'].to_s.to_sym][:nam] = row['file']
        temp[row['id'].to_s.to_sym][:file] = Wikimedia.thumbnail(row['file'])
      end

      self
    end

    # The id of the phrase
    # @return [Integger]
    def phrase_id
      @phrase_id
    end

    # The id of given image
    # @param [Integer] offset
    # @return [Image]
    def image_id(offset)
      @images[offset][:id]
    end

    # Check the validity of the answer
    # @param answer [Integer]
    # @return [true|false]
    def answer_correct?(answer)
      get_images unless @files
      ret = false

      if answer >= @images.size
        raise RangeError
      end

      @images.each_with_index do |v, i|
        if v[:correct] && i == answer
          ret = true
        end
      end

      ret
    end

    # An array of image
    # @return [Array<Image>]
    def images
      get_images unless @files

      @images.map { |i| i[:file] }
    end

    # The language of the question
    # @return [Language]
    def language
      if @language.nil?
         get_phrase
         @language = Vortprovizo::Languages.get(@lang_id)
      end
      @language.english
    end
  end

  class Card
    # Creat a Quiz Card from a given id
    class OldCard < Card
      # @param [String] id A string that describes a particular Card
      def initialize(id: nil)
        super

        begin
          byte_string = Base64.strict_decode64(id)
        rescue StandardError => e
          raise "Bad Data"  # FIXME
        end
        data = byte_string.unpack('L*')
        @phrase_id = data[0]
        @audio_id = data[1]
        @images = Array.new
        data[2..-1].each do |d|
          @images.push Hash[ id: d >> 1, correct: d % 2 == 1 ]
        end
      end
    end

    # Creat and random Quiz Card
    class RandCard < Card
      # @param [String] language
      def initialize(language: 'en')
        l = Vortprovizo::Languages.get(language)
        @language = l
        @lang_id = l.id
        connection = Vortprovizo::Pg.connection

        rd = connection.exec_params(<<~SQL, [ @lang_id ])
          select p.id
               , p.phrase
            from phrases p
           inner join phrase_image pi
              on p.id = phrase_id
           where p.language_id = $1
           group by p.id, p.phrase
           order by random()
           limit 1
        SQL

        @phrase = rd[0]['phrase']
        @phrase_id = rd[0]['id'].to_i

        rd = connection.exec_params(<<~SQL, [ @phrase_id ])
          SELECT a.id, file
            FROM phrase_audio pa
            JOIN audio a
              ON pa.audio_id = a.id
           WHERE pa.phrase_id = $1
           ORDER by random()
           LIMIT 1
        SQL
        if rd.cmd_tuples > 0
          @audio = rd[0]['file']
          @audio_id = rd[0]['id'].to_i  if rd[0]['id']
          @audio_path = Wikimedia.audio(@audio)
        end

        rd = connection.exec_params(<<~SQL, [ @phrase_id ])
          SELECT i.id, file
            FROM phrase_image pi
            JOIN images i
              ON pi.image_id = i.id
           WHERE pi.phrase_id = $1
           ORDER BY random()
           LIMIT 1
        SQL

        @file = Wikimedia.thumbnail(rd[0]['file'])
        @file_id = rd[0]['id'].to_i

        @files = Array[ Hash[ file: Wikimedia.thumbnail(rd[0]['file']), id:  rd[0]['id'].to_i, correct: true ] ]

        rd = connection.exec_params(<<~SQL, [ ])
          SELECT i.id image_id
               , i.file
            FROM images i
            JOIN phrase_image pi
              ON i.id = pi.image_id
             and pi.phrase_id != 1241
           order by random()
           limit 3
        SQL
        @files += rd.map { |r| Hash[file: Wikimedia.thumbnail(r['file']), id: r['image_id'].to_i, correct: false ] }
        @images = @files.shuffle
      end
    end
  end
end

__END__

