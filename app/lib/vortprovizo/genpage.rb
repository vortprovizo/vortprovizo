# frozen_string_literal: true

require 'nokogumbo'

class Vortprovizo
  # The genpage class loads html for a laguage and update the user header
  class Genpage
    # @param lang is the list of user languages.
    def initialize(lang = 'en')
      @lang = lang
    end

    # @param filename
    # @return Nokogiri document
    # @raise parsing errors
    def genpage(filename)
      begin
        file = File.open("../html/%s/%s.html" % [ @lang, filename ], 'r:UTF-8')
        doc = Nokogiri::HTML5(file)
        return doc
      rescue Errno::ENOENT => e
        raise ArgumentError.new('File not found %s [%s]' % [ filename, e ])
      end
    end

    # @param filename
    # @return Nokogiri document
    # @raise parsing errors
    def self.genpage(filename)
      file = File.open("../html/en/%s.html" % [ filename ], 'r:UTF-8')
      begin
        doc = Nokogiri::HTML5(file)
        return doc
      rescue => e
        puts e.class
        puts e.inspect
        return nil
      end
    end
  end
end

__END__

