# frozen_string_literal: true

require 'digest'
require 'nokogumbo'
require 'vortprovizo/pg'
require 'vortprovizo/languages'
require 'vortprovizo/card'
require 'vortprovizo/db/bad_phrase_image'
require 'wikimedia'

class Vortprovizo

  # The quise for unlogged in users
  class Demo

    def initialize(language: 'cho', session: 'none')
      @lang_id = language
      @session = session.session_id
    end

    # Get the quiz
    def get_quiz(id: nil, answer: nil, next_id: nil)
      cc = nil
      if (id)
        pc = Vortprovizo::Card::OldCard.new(id: id)
        user_answer = answer.ord - 'A'.ord
        if (pc.answer_correct? user_answer) 
          puts "GOOD"
        else
          puts "BAD"
          Vortprovizo::Db::BadPhraseImage.set_bad(pc.phrase_id, pc.image_id(user_answer))
        end
      end
      if (next_id)
        cc = Vortprovizo::Card::OldCard.new(id: next_id)
      else
        cc = Vortprovizo::Card::RandCard.new(language: @lang_id)
      end
      nc = Vortprovizo::Card::RandCard.new(language: @lang_id)

      data = {
	phrase: cc.phrase,
	audio: cc.audio_path,
	audio_type: 'application/ogg',
	images: cc.images,
	next_images: nc.images,
	next_audio: nc.audio_path,
	next_audio_type: 'application/ogg',
	next_id: nc.id,
	id: cc.id,
	language: cc.language
      }

      return data
    end

    # generate a new quiz
    def get_first_quiz()
      cc = Vortprovizo::RandCard.new(language: @lang_id)
      nc = Vortprovizo::RandCard.new(language: @lang_id)

      data = {
	phrase: cc.phrase,
	audio: cc.audio_path,
	audio_type: 'application/ogg',
	images: cc.images,
	next_images: nc.images,
	next_audio: nc.audio_path,
	next_audio_type: 'application/ogg',
	next_id: nc.id,
	id: cc.id,
	language: cc.language
      }

      return data
    end
  end
end

__END__

