# frozen_string_literal: true

require 'digest'
require 'vortprovizo/pg'

class Vortprovizo
  # An object thet describes a *User*.
  class User
    attr_reader :id
    attr_reader :username
    attr_reader :name
    def initialize(id: nil, username: nil, name: nil)
      @id = id
      @username = username
      @name = name
    end

  end
  # Vortprovizo uses a session cookie to hold information about the current users and session
  class Session
    attr_reader :session_id
    attr_reader :session_key
    def new_session?
      @new_session
    end

    def loggedin?
      !@user_id.nil?
    end

    # Logout the user from the session
    def logout
      connection = Vortprovizo::Pg.connection
      rd = connection.exec_params(<<~SQL, [ @session_id ])
        UPDATE sessions
           SET user_id = NULL
         WHERE id = $1
      SQL
      if (rd.cmd_tuples < 1)
         # warn
      end
    end

    # Get the csrf_tokens for a given `session_id`
    # @param session_id[Number]
    def get_csrf_tokens(session_id = @session_id)
      connection = Vortprovizo::Pg.connection
      rd = connection.exec_params(<<~SQL, [ session_id ])
        SELECT secret
             , expires
             , expires - now() as interval
             , (extract(epoch FROM expires - now()) / 43200)::int - 1 as age
             , expires - now() as interval
          FROM session_csrf
         WHERE session_id = $1
           AND expires > now()
         ORDER by expires desc
      SQL
      rd.each do |item|
	csrf = Digest::SHA2.new(512).base64digest(item['secret'].to_s)
	@csrf_valid[csrf] = csrf
	if (item['age'].to_i == 1)
	  @csrf_current = csrf
	end
      end
      unless @csrf_current
	secret = Random.rand()
#	csrf = Digest::SHA2.new(512).base64digest(secret.to_s)
	connection.transaction do |conn|
	  rd = conn.exec_params(<<~SQL, [ secret, @session_id ])
            UPDATE session_csrf
               SET expires = now() + interval '1 day'
                 , secret = $1
             WHERE id IN (
              select id
                from session_csrf  
               where (extract(epoch FROM expires - now()) / 43200)::int - 1 < 0
                 and session_id = $2
               order by (extract(epoch FROM expires - now()) / 43200)::int - 1
               limit 1
             )
          SQL
	  rd = conn.exec_params(<<~SQL, [ @session_id, secret ])
            INSERT into session_csrf (session_id, secret, expires)
             select $1, $2, now() + interval '1 day'
              WHERE not exists (
                select 1 from session_csrf where expires = now() + interval '1 day'
              )
          SQL
        end
      end
    end

    # check_csrf_token
    def check_csrf_token(token)
      get_csrf_tokens
    end

    # get a rand base64digest
    def get_csrf_token
      get_csrf_tokens
      unless @csrf_token
      end
      Digest::SHA2.new(512).base64digest(Random.rand().to_s)
    end

    def initialize(string_id)
      @csrf_valid = Hash.new
      connection = Vortprovizo::Pg.connection
      @session_id = nil
      if string_id
        rd = connection.exec_params(<<~SQL, [ string_id ])
          SELECT s.id as session_id
               , s.session_key as session_key
               , u.id as user_id
               , u.user_name as user_name
               , s.created_date as session_date
               , u.last_login
            FROM sessions s
            LEFT JOIN users u
              ON s.user_id = u.id
           WHERE session_key = $1
        SQL
        puts rd.cmd_tuples
	if rd.cmd_tuples > 0
	  @session_id   = rd[0]['session_id']
	  @session_key  = rd[0]['session_key']
	  @user_id      = rd[0]['user_id']
	  @user         = rd[0]['user_name']
	  @last_login   = rd[0]['last_login']
	  @session_date = rd[0]['session_date']
	end
      end

      if @session_id.nil?
	@session_key = Digest::SHA1.base64digest(Random.rand().to_s)
        rd = connection.exec_params('insert into sessions (session_key) values ($1) returning id', [ @session_key ])
	@session_id = rd[0]['id']
	@new_session = true
      else
	@new_session = false
      end
    end

    # create a user
    def create_user(full_name: nil, user_name: 'asdf', password: nil, email: nil, email_opted_in: nil)
      connection = Vortprovizo::Pg.connection
      connection.transaction do |conn|
	rd = conn.exec_params(<<~SQL, [ full_name, user_name, password ])
          INSERT INTO users (full_name, user_name, password) values ($1, $2, crypt($3::text, gen_salt('bf')))
        SQL
        rd = conn.exec_params(<<~SQL)
          SELECT currval(pg_get_serial_sequence('users','id')) as user_id
        SQL
        user_id = rd[0]['user_id']
        email_hash = Digest::SHA256.base64digest(email)
        rd = conn.exec_params(<<~SQL, [ user_id, email_hash ])
          INSERT INTO emails (user_id, email_hash) values ($1, $2)
        SQL
      end
    end

    # Using and email address find a user
    # @param email[String]
    def get_user_from_email(email)

      email_hash = Digest::SHA256.base64digest(email)

      connection = Vortprovizo::Pg.connection
      rd = connection.exec_params(<<~SQL, [ email_hash ])
        SELECT user_id
          FROM emails
         WHERE email_hash = $1
      SQL

      if (rd.cmd_tuples > 0)
        return rd[0]['user_id']
      else
        return nil
      end
    end
    
    # get the user by there id and password
    def check_user_by_id(user_id = nil, password = nil)
      connection = Vortprovizo::Pg.connection
      if password
        rd = connection.exec_params(<<~SQL, [ user_id, password ])
          SELECT id 
            FROM users
           WHERE id = $1
             AND password = crypt($2, password)
        SQL
      else
        rd = connection.exec_params(<<~SQL, [ user_id ])
          SELECT id 
            FROM users
           WHERE id = $1
        SQL
      end

      if (rd.cmd_tuples > 0)
        @user_id = rd[0]['id']
	@user = rd[0]['user_name']
	rd = connection.exec_params(<<~SQL, [ @session_id, @user_id ])
          update sessions
             set user_id = $2
           where id = $1
        SQL
      end
    end


    # get the user by there email and password
    def check_user_from_email(email: nil, password: nil)
      user_id = self.get_user_from_email(email)
      check_user_by_id(user_id, password)
    end

    # check the user_name password pair
    # @param user_name[String]
    # @param password[String]
    # @return [True|False]
    def check_user(user_name: nil, password: nil)
      connection = Vortprovizo::Pg.connection
      rd = connection.exec_params(<<~SQL, [ user_name, password ])
        SELECT id 
          FROM users
         WHERE user_name = $1
           AND password = crypt($2, password)
      SQL
      if (rd.cmd_tuples > 0)
        @user_id = rd[0]['id']
	@user = rd[0]['user_name']
	rd = connection.exec_params(<<~SQL, [ @session_id, @user_id ])
          update sessions
            set user_id = $2
          where id = $1
        SQL
        true
      else
        false
      end
    end

    # get a session cookie 
    # @param age[Number]
    # @return [HTTP::Cookie]
    def get_cookie(age = 7)
      HTTP::Cookie.new('sessionId', @session_key, path: '/', max_age: age * 86_400)
    end
  end
end

