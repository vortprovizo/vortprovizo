# frozen_string_literal: true

require 'pg'
require 'set'

class Vortprovizo
  # Singleton used to get connections to the database
  # @see https://www.rubydoc.info/gems/pg/PG#connect-class_method
  # @see https://www.rubydoc.info/gems/pg/PG/Result
  class Pg
    @@connection = nil
    @@keys = Set.new
    # Get a connection
    # @param type [:rw|:ro]
    # @return [Pg::Connection]
    def self.connection(type = :rw)
       unless @@connection
	 @@connection = PG.connect :dbname => 'vortprovizo'
	 @@connection.exec_params('SET client_encoding = UTF8;', [ ])
       end
       return @@connection
    end
    # Create prepared statements
    # @param name [String] A Postgresql name
    # @param sql [String<SQL>] A Postgresql parsable string
    def self.add_statment(name, sql)
#      unless @@keys.add?(name)
	connection.prepare(name, sql)
#      end
    end
  end
end

