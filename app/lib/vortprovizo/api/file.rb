# frozen_string_literal: true

require 'minitest/mock'
require 'vortprovizo/api'
require 'vortprovizo/db/file'
require 'wikimedia'

# End point for Adding Image or Audio files
class Vortprovizo::Http::Api
  class File < self
    register_routes '/api/v1/file'

    # @private
    def self.add_file(phrase_id, name)
      ret = nil
      
      begin
        if (m = name.match(/(Q[0-9]*)/))
          name = m[1]
        elsif (m = name.match(%r{/([^/]+)$}))
          name = m[1]
        elsif (m = name.match(/[A-Za-z]+:(.*)$/))
          name = m[1]
        end
        type = ::Wikimedia.check_type(name)
        case type
        when :entity
          filename = Wikimedia.check_entity(name)
          ret = Vortprovizo::Db::File.add_entity(filename, phrase_id)
        when :image
          filename = Wikimedia.check_image(name)
          ret = Vortprovizo::Db::File.add_image_file(filename, phrase_id)
        when :audio
          filename = Wikimedia.check_audio(name)
          ret = Vortprovizo::Db::File.add_audio_file(filename, phrase_id)
        else
          raise "Unknown type"
        end
      rescue PG::UniqueViolation => e
        raise 'no file ' + e.to_s 
      end
      return ret
    end

    # @private
    def add_file(phrase_id, filename)
      self.class.add_file(phrase_id, filename)
    end

    # Create a file of unkwon type
    def post(phrase_id: nil, file: nil)
      filename_clean = CGI.unescape(file)
      if filename_clean == '_test'
        return {
          'id' => '1735',
          'phrase' => 'het spinnenweb',
          'iso2' => 'nl',
          'iso3' => 'nld',
          'language' => 'nld',
        }
      end
      ret = add_file(phrase_id, filename_clean)

      return ret 
    end
  end
end
