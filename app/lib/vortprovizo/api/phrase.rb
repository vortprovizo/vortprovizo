# frozen_string_literal: true

require 'vortprovizo/db/file'
require 'vortprovizo/api'
require 'vortprovizo/languages'
require 'http'
require 'json'
require 'cgi'

class Vortprovizo::Http::Api
  # End point for Phrase
  class Phrase < self
    register_routes '/api/v1/phrase', '/api/v1/phrases'

    # Remove the _Phrase_ from the database if it is not being used
    def delete(phrase_id: nil)
      x = Vortprovizo::Db::File.delete_phrase(phrase_id)
      return x
    rescue StandardError => e
      return { 'error': e }
    end

    # Add a _Phrase_ to  the database if it does not already exist.
    # @raise DuplicateError
    def post(phrase: nil, language: nil)
      language_obj = Vortprovizo::Languages.get(language)

      raise Error unless phrase && phrase.size > 0

      x = Vortprovizo::Db::File.add_phrase(phrase, language_obj)

      return x
    end
  end
end
