# frozen_string_literal: true

require 'digest'
require 'nokogumbo'

require 'vortprovizo/pg'
require 'wikimedia'
require 'vortprovizo/api'
require 'vortprovizo/demo'

# Demo Api endpoint
class Vortprovizo::Http::Api
  class Demo < self
    register_routes '/api/v1/demo'

    # Get the data for a quiz
    def post(a)
      question = a[:question]
      answer = a[:answer]
      next_id = a[:next]

      ret = Vortprovizo::Demo.new(language: 'nl', session: session()).get_quiz(
        id: question,
        answer: answer,
        next_id: next_id,
      )
      return ret
    end

    # FIXME: don't know what
    def get(_env)
      ret = Vortprovizo::Demo.new(language: 'nl', session: session()).get_quiz()
      return ret
    end
  end
end

__END__

