# frozen_string_literal: true

require 'oauth2'
require 'net/http'
require 'vortprovizo/http/api'
require 'vortprovizo/oidc'

class Vortprovizo::Http::Api
  # A class
  class Oath2 < self
    # puts Oath2.ancestors
    register_routes '/api/v1/oauth2/{name}'

    # GET call for oauth2
    def get(data)
      code = data.fetch('code', [ ]).first
      state = data.fetch('state', [ ]).first
      name = data.fetch('name', [ ]).first

      oidc = Vortprovizo::OIDC.oauth(name.to_sym)
      client = Vortprovizo::OIDC.client(name.to_sym)
      x = Vortprovizo::OIDC.oauth(name.to_sym)

      access = nil
      ret = nil
      begin
        code_verifier = x.code_verifier
        code_challenge = Base64.urlsafe_encode64(Digest::SHA256.digest(code_verifier)).chomp('=')

        begin
          access = client.auth_code.get_token(
            data['code'][0],
            code_verifier: code_verifier,
          )
        rescue => e
          raise e
          # return [ 409, { }, [ e.inspect ] ]
        end
        uri = URI.parse(oidc.userinfo_endpoint)

        Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
          ret = http.request_get(uri.request_uri, Authorization: 'Bearer %s' % [ access.token ] )
        end
        puts "------------%s----------" % [ ret.code ]
        if ret.code.to_i == 200
          puts "----------------------"
          data = JSON.parse(ret.body)
#          pp data
          session.check_user_from_email(email: data['email'])
        end
      rescue => e
        access = e
        return [ 500, { 'content-type' => 'text/html' }, [ 'error %s %s' % [ 'bob', e ] ] ]
      end

      return [ 303, { 'Location' => '/index.html', 'content-type' => 'text/html' }, [ 'bob' ] ]
    end
  end
end
