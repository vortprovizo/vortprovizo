# frozen_string_literal: true

class Vortprovizo
  # Miscellaneous helper static methods
  class Tools
    # Parse the preferred languages string of a web browser
    def self.parse_accept_language(language_list)
      ret = Array.new
      ret = language_list.split(/,/).map { |p| p.split(';').first }
      return ret
    end
  end
end

__END__

