# frozen_string_literal: true

require 'vortprovizo/db/image'
require 'vortprovizo/db/phrases'

# contains a list of `Images`
# in an  [Array](https://ruby-doc.org/core-3.0.0/Array.html)
class Vortprovizo::Db::Images < Array

  # @param [Array[Int]] image_ids A list of `Image` ids
  def self.[](*image_ids)
    raise "Not an Array" unless image_ids.is_a?(Array)
    raise 'Only integers' if image_ids.select { |i| !i.is_a?(Integer) }.size > 0

    super(*image_ids)
  end

  # @return [Enumerable<Image>] images
  def each
    return enum_for(:each) unless block_given? 
    super do |id|
      yield(Vortprovizo::Db::Image.get_by_id(id: id))
    end
  end

  # return a list of phrases by image_id 
  def all_linked_phrases
     Vortprovizo::Db::Phrases.get_by_image_ids(to_a)
  end

end
