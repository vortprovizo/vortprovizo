# frozen_string_literal: true

require 'wikimedia'
require 'vortprovizo/pg'

class Vortprovizo
  module Db
    # Higher level storage functions for the Vortprovizo Language Learning System
    class BadPhraseImage
      # @param phrase_id [Integer]
      # @param image_id [Integer]
      # @return [BadPhraseImage]
      def self.set_bad(phrase_id, image_id)
	conn = Vortprovizo::Pg.connection
        rs = conn.exec(<<~SQL, [ phrase_id, image_id ])
          INSERT INTO bad_phrase_image (phrase_id, image_id) VALUES($1, $2)
          ON CONFLICT(phrase_id, image_id)
          DO
             UPDATE SET bad_count = bad_phrase_image.bad_count + 1
        SQL
        self
      end
    end
  end
end
