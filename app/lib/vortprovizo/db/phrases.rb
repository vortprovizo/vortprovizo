# frozen_string_literal: true

require 'vortprovizo'
require 'vortprovizo/db/images'

module Vortprovizo::Db
# This class holdes a list of phrases where each phrase is held in a Vortprovizo::Db::Phrases Object
class Phrases < Array

  # return the images linked together by a phrase
  def all_linked_phrases
    [ ]
  end

  # return the phrase by id
  def self.get_from_ids(ids: [ ])
    conn = Vortprovizo::Pg.connection
    ret = self.new()
    if (ids.size > 0)
      rs = conn.exec(<<~SQL % [ ids.each_index.map { |i| ('$%s' % [ i + 1 ]) }.join(',') ], ids )
      select p.id
           , phrase
           , coalesce(ac.count, 0) a_cnt
           , coalesce(ic.count, 0) p_cnt
        from phrases AS p
        left join (
          select phrase_id as id, count(*) count from phrase_image
          group by phrase_id
        ) AS ic
          on p.id = ic.id
        left join (
          select phrase_id as id, count(*) count from phrase_audio
          group by phrase_id
        ) as ac
          on p.id = ac.id
       where p.id in (%s)
       order by phrase
      SQL
      rs.each do |r|
        ret.push Vortprovizo::Db::Phrase.new(id: r['id'], phrase: r['phrase'], p_count: r['p_cnt'], a_count: r['a_cnt'])
      end
    end

    return ret
  end

  # Get all the phrases for a particular language
  # @return [Vortprovizo::Db::Phrases]
  def self.get_language(language_id: nil)
    conn = Vortprovizo::Pg.connection
    rs = conn.exec(<<~SQL, [ language_id ])
    select p.id
         , phrase
         , coalesce(ac.count, 0) a_cnt
         , coalesce(ic.count, 0) p_cnt
      from phrases AS p
      left join (
        select phrase_id as id, count(*) count from phrase_image
        group by phrase_id
      ) AS ic
        on p.id = ic.id
      left join (
        select phrase_id as id, count(*) count from phrase_audio
        group by phrase_id
      ) as ac
        on p.id = ac.id
     where language_id = $1
     order by phrase
    SQL

    return self[* rs.map { |r| Vortprovizo::Db::Phrase.new( id: r['id'], phrase: r['phrase'], a_count: r['a_cnt'], p_count: r['p_cnt'] ) } ]
  end

  # get all ids for language
  def ids()
    each.map { |i| i.id }
  end

  # get all ids for language
  def ids_index()
    each_index.map { |i| 1 +  i }
  end

  # return the images linked together by a phrase
  def all_linked_images()
    conn = Vortprovizo::Pg.connection
    ret = nil

    if (size > 0)
      sql = <<~SQL % [ ids_index.map { |i| '$%d' % [ i ] }.join(',') ]
      select image_id
        from phrase_image
       where phrase_id in (%s)
       group by image_id
      SQL

      my_ids = ids()

      rs = conn.exec(sql, my_ids.to_a)

      ret = Vortprovizo::Db::Images[ *rs.map { |row| row['image_id'].to_i } ]
    else
      ret = Vortprovizo::Db::Images.new()
    end 
    ret 
  end

  # given an image id get phrases
  def self.get_by_image_ids(ids)
    conn = Vortprovizo::Pg.connection
    ret = nil

    if (ids.size > 0)
      sql = <<~SQL % [ ids.each_with_index.map { |_, i| '$%d' % [ i+1 ] }.join(',') ]
      select phrase_id
        from phrase_image
       where image_id in (%s)
       group by phrase_id
      SQL

      rs = conn.exec(sql, ids)

      ret = Vortprovizo::Db::Phrases[ *rs.map { |row| row['phrase_id'].to_i } ]
#      pp ret
      ret
    else
      ret = Vortprovizo::Db::Phrases.new()
    end 

    new()
  end

end
end
