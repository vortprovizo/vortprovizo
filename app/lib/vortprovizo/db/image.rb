# frozen_string_literal: true

require 'vortprovizo/db'
require 'vortprovizo/pg'
require 'vortprovizo/db/phrases'

# This class hold an image
class Vortprovizo::Db::Image
  # The unique id of the image
  attr_reader :id
  # The file name of the image
  attr_reader :file
  def initialize(id: nil, file: nil)
    @id = id
    @file = file
  end
  # get an imagea by id
  # @param [Int] id
  # @return [Vortprovizo::Db::Image]
  def self.get_by_id(id: nil )
    conn = Vortprovizo::Pg.connection
    rd = conn.exec(<<~SQL, [ id ])
      select i.id as id, file
      from images i
      where id = $1
    SQL
    if ( rd.ntuples == 0 )
      raise 'NotFound'
    end
    row = rd.first

    ret = new(id: row['id'], file: row['file'])
    return ret
  end

  # Return a list of all phrases that use this image
  # @return [Vortprovizo::Db::Phrases]
  def all_linked_phrases
    conn = Vortprovizo::Pg.connection

    rd = conn.exec(<<~SQL, [ @id ])
      select phrase_id
      from phrase_image
      where image_id = $1
    SQL
    list = rd.map{ |r| r['phrase_id'] }
    
    ret = Vortprovizo::Db::Phrases.get_from_ids(ids: list)

   ret
  end
end
