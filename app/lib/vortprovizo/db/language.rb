# frozen_string_literal: true

require 'vortprovizo/pg'

# All the Database requests are in this module
module Vortprovizo::Db
  # Access the _languages_ database table
  class Language
    # get all the languages
    def self.get_all
      conn = Vortprovizo::Pg.connection
      rd = conn.exec_params(<<~SQL, [ ])
        SELECT id, iso2, iso3, name, rtl, note  from languages
      SQL
      rd.map{ |h| h.transform_keys(&:to_sym) }
    end
  end
end
