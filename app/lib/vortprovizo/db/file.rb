# frozen_string_literal: true

require 'wikimedia'
require 'vortprovizo/pg'
require 'vortprovizo/error'

module Vortprovizo::Db
  # Duplicate entry on insert
  class DuplicateError < Vortprovizo::Error::DuplicateEntry; end

  # Higher level storage functions for the Vortprovizo Language Learning System
  class File
    # Add audio file
    # @param filename
    # @param phrase_id
    # #param conn
    def self.add_audio_file(filename, phrase_id, conn = Vortprovizo::Pg.connection )
      rd = conn.exec_params(<<~SQL, [ phrase_id ])
      SELECT id, language_id from phrases where id = $1
      SQL
      language_id = rd[0]['language_id']
      rd = conn.exec_params(<<~SQL, [ filename, language_id ])
      INSERT INTO audio (file, language_id) values ($1, $2)
      SQL
      rd = conn.exec_params(%q|SELECT currval(pg_get_serial_sequence('audio','id')) as audio_id|)
      audio_id = rd[0]['audio_id']
      rd = conn.exec_params(<<~SQL, [ phrase_id, audio_id ])
      INSERT INTO phrase_audio (phrase_id, audio_id) values ($1, $2)
      SQL
    end

    # Add an Entity
    def self.add_entity(entity_name, phrase_id)
      conn = Vortprovizo::Pg.connection

      entity_id = entity_name[1..-1]
      puts "name %s filename %s" % [ entity_name, entity_id ]

      begin
        conn.exec_params(<<~SQL, [ entity_id, phrase_id ])
          INSERT INTO wikientity (entity_id, phrase_id) values ($1, $2)
        SQL
      rescue PG::UniqueViolation => e
        raise DuplicateError.new(
          exception: e,
          item: :wikientity,
          msg: "'%s' '%s' all ready exists (%s)" % [ phrase_id, entity_name, e ],
          type: :phrase,
        )
      end
    end

    # Add the filename to the database
    # @param filename
    # @param phrase_id
    # @raise NO_FILE

    def self.add_image_file(filename, phrase_id, conn = Vortprovizo::Pg.connection)
      begin
        rd = conn.exec_params(<<~SQL, [ filename ])
          INSERT INTO images (file) values ($1) returning id
        SQL
      rescue => x
        puts "ERROR: '%s'" % [ x ]
        rd = conn.exec_params(<<~SQL, [ filename ])
          SELECT id FROM images where file = $1
        SQL
      end
      image_id = rd[0]['id']
      rd = conn.exec_params(<<~SQL, [ phrase_id, image_id ])
        INSERT INTO phrase_image (phrase_id, image_id) values ($1, $2)
      SQL
    end

    # Given the URI or name of a *Wikipedia* `File:` determine if it is an audio file
    # or an image file and associate the file with a [Db::Phrase].

    def self._add_file(phrase_id, filename)
      ret = nil
      
      begin
        if (m = filename.match(/(Q[0-9]*)/))
          filename = m[1]
        elsif (m = filename.match(%r{/([^/]+)$}))
          filename = m[1]
        elsif (m = filename.match(/[A-Za-z]+:(.*)$/))
          filename = m[1]
        end
        type = ::Wikimedia.check_type(filename)
        case type
        when :entity
          ret = self.add_entity(filename, phrase_id)
        when :image
          ret = self.add_image_file(filename, phrase_id)
        when :audio
          ret = self.add_audio_file(filename, phrase_id)
        else
          raise "Unknown type"
        end
      rescue PG::UniqueViolation => e
        raise 'no file ' + e.to_s 
      end
      return ret
    end

    # Delete a phrase from the dictionary
    # @param phrase_id [Interger]

    def self.delete_phrase(phrase_id)
      conn = Vortprovizo::Pg.connection
      
      rd = begin
        conn.exec_params(<<SQL, [ phrase_id ])
DELETE FROM phrases where id = $1
SQL
      rescue => x
        raise "Error: '%s'" % [ x ] 
      end

      Hash[
        'count' => rd.cmd_tuples
      ]

    end

    # Add a phrase to the dictionary
    # @param phrase [String]
    # @param language [Vortprovizo::Language]

    def self.add_phrase(phrase, language, conn = Vortprovizo::Pg.connection)

      rd = begin
        conn.exec_params(<<SQL, [ phrase, language.id  ])
INSERT INTO phrases (phrase, language_id) values ($1, $2) returning id
SQL
      rescue PG::UniqueViolation => e
        raise DuplicateError.new(
          exception: e,
          item: phrase,
          msg: "'%s' all ready exists (%s)" % [ phrase, e ],
          type: :phrase,
        )
      end

      Hash[
        'id' => rd[0]['id'],
        'phrase' => phrase,
        'iso2' => language.iso2,
        'iso3' => language.iso3,
        'language' => language.iso3,
      ]
    end
  end
end
