# frozen_string_literal: true

require 'wikimedia'
require 'vortprovizo/pg'

module Vortprovizo::Db
  # Higher level storage functions for the Vortprovizo Language Learning System
  class Phrase
    def initialize(data)
      @data = data
    end

    # Return the phrase for the Phrase
    # @return [String]
    def phrase
      @data[:phrase]
    end

    # Return the number of audio files attached to the Phrase
    # @return [Number]
    def a_cnt
      @data[:a_count]
    end

    # @return [Number]
    def p_cnt
      @data[:p_count]
    end

    # @return [Number]
    def id
      @data[:id]
    end

    # retrieve a phrase by phrase or id and language
    def self.get(phrase: nil, id: nil, language: nil)
      conn = Vortprovizo::Pg.connection
      rs = nil
      if id
        rs = conn.exec_params('select id, phrase, language_id from phrases where id = $1', [ id ])
        if rs.cmd_tuples > 0
          language = Vortprovizo::Languages.get(rs[0]['language_id'])
          phrase = rs[0]['phrase']
        end
      else
        rs = conn.exec_params('select id, phrase from phrases where phrase = $1 and language_id = $2', [ phrase, language.id ])
      end

      ret = Hash[ phrase: phrase, language: language.iso3 ]

      if (rs.cmd_tuples > 0)
        id = ret[:id] = rs[0]['id']

        rd = conn.exec('select a.id, a.file from phrase_audio pa join audio a on a.id = pa.audio_id  where phrase_id = $1', [ id ])
        ret[:audio_files] = rd.to_a

        rd = conn.exec('select i.id, i.file from phrase_image pi join images i on i.id = pi.image_id  where phrase_id = $1', [ id ])
        ret[:image_files] = rd.to_a

        new(ret)
      else
        raise "NotFound: %s -" % [ phrase ]
      end

      return ret
    end

    # get phrase by name
    def self.get_phrase_by_name(phrase, language_id)

      rs = @conn.exec('select id, phrase from phrases where phrase = $1 and language_id = $2', [ phrase, language_id ])

      ret = Hash[ phrase: phrase, language: 'nld' ]

      if (rs.cmd_tuples > 0)
        id = ret['phrase_id'] = rs[0]['id']

        rd = @conn.exec('select a.id, a.file from phrase_audio pa join audio a on a.id = pa.audio_id  where phrase_id = $1', [ id ])
        ret['audio_files'] = rd.to_a

        rd = @conn.exec('select i.id, i.file from phrase_image pi join images i on i.id = pi.image_id  where phrase_id = $1', [ id ])
        ret['image_files'] = rd.to_a
      else
        ret['error'] = 'Phrase not found'
      end
      return ret
    end
  end
end
