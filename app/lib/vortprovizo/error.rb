# frozen_string_literal: true

class Vortprovizo
  # all of the Errors in Vortprovizo
  module Error
    # An General Error
    class Error < ::StandardError
    end
    # A Duplicate Error
    class DuplicateEntry < ::StandardError
    end
  end
end
