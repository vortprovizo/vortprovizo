# frozen_string_literal: true

require 'vortprovizo'
require 'vortprovizo/db/file'
require 'vortprovizo/languages'
require 'json'
require 'cgi'

class Vortprovizo::Http
  # Class for all API (json) file
  # @abstract
  class Api < self
    # @private
    def initialize(env)
      super

      query_string = env['QUERY_STRING']
      @params = CGI.parse(query_string || '')
      @set_cookies = Array[]
      cookies =
        begin
          if env['HTTP_COOKIE']
            HTTP::Cookie.cookie_value_to_hash(env['HTTP_COOKIE'])
          end
        rescue StandardError => e
          warn 'Cookie Error: ' + e.to_s + ' - ' + env['HTTP_COOKIE'].to_s + e.backtrace.to_json
          ''
        end
      if (session_id = cookies && cookies['sessionId'])
        @session = Vortprovizo::Session.new(session_id)
      end
    end

    # @!group Abstract

    # @abstract override this method
    def get
      raise NotFoundError.new("You must define the `get` method for %s" % [ self.class.to_s ])
    end

    # @abstract override this method
    def post
      raise NotFoundError.new("You must define the `post` method for %s" % [ self.class.to_s ])
    end

    # @abstract override this method
    def delete
      raise NotFoundError.new("You must define the `delete` method for %s" % [ self.class.to_s ])
    end

    # @!endgroup

    # get the session key data
    # @return [Vortprovizo::Session]
    attr_reader :session

    # Call the Rack application defined by this Mapping.
    # @return [Array|String] The Rack return value
    def call(_env = nil)
      post_data = @env['rack.input'].read.force_encoding('utf-8')
      @post_params = @env['post_params'] =
        if @env['CONTENT_TYPE']&.match(/json/)
          JSON.parse(post_data)
        else
          CGI.parse(post_data)
        end

      params = @post_params.transform_keys(&:to_sym)

      begin
        ret = nil
        case @env['REQUEST_METHOD']
        when 'GET'
          ret = get(@params)
        when 'POST'
          ret = post(params)
          [ 201, {}, [ ret.to_json ] ]
        when 'DELETE'
          puts 'DELETE'
          ret = delete(params)
          return ret
        else
          raise 'unknown type %s' % [ @env['REQUEST_METHOD'] ]
        end
      rescue Vortprovizo::Error::DuplicateEntry => e
        puts '409: %s' % [ e ]
        [ 409, {}, [ '{ "error":"%s" }' % [ e.message ] ] ]
      rescue StandardError => e
        puts '400: %s' % [ e ]
        puts e.backtrace
        [ 400, {}, [ '{ "error":"%s" }' % [ e ] ] ]
      end

      case ret
      when Array
        ret
      else
        [ 200, {}, [ ret.to_json ] ]
      end
    end
  end
end
