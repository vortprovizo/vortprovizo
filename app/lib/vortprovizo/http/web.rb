# frozen_string_literal: true

require 'cgi'
require 'http-cookie' #require 'json'
require 'vortprovizo'
require 'vortprovizo/session'
require 'vortprovizo/utils'
require 'vortprovizo/genpage'

class Vortprovizo::Http
  # @abstract Subclass and override {#get} to implement
  # This object handles Web based queries
  # That is ones that come from a web browser and are inteded to render a human readable web page.
  class Web < self
    attr_accessor :set_cookies
    attr_accessor :session
    attr_accessor :languages

    register_routes('/test/web')

    # @private
    def self.register_file(file)
      @@file = file
    end

    # @abstract Subclasses should override this method to generate a web page
    # @return String, Array[ Int, Hash, Array ]
    def get
      raise NotFoundError.new("You must define the `get` method for %s" % [ self.class.to_s ])
    end

    # this method break down the maping
    # @return [Array]
    def call(_env = nil)

      ret = begin

        ret = get()

        case ret
        when String
          if ret[1].is_a?(Hash)
            ret[1]['Set-Cookie'] = app.set_cookies.map { |c| c.set_cookie_value }.join(', ')
          end
          ret.force_encoding('utf-8')
          return [ 200, { 'content-type' => 'text/html; charset=utf-8;' }, [ ret ] ]
        when Array
          ret
        when Nokogiri::HTML::Document
          ret = [ 200, { 'content-type' => 'text/html; charset=utf-8;' }, [ ret.to_xml ] ]
        else
          ret = [ 200, {}, [ ret.to_xml ] ]
        end
      rescue NotFoundError => e
        if ( self.development? )
          puts "development?"
        end
        raise e
      end

      return ret
    end

    # Get the Nokogiri object for a given file
    #
    # @param language [Language] the list of users languages
    # @param path [path] path to the fil
    #
    # @return [Nokogiri::XML] the object page in Nokogiri .
    def genpage(language, path)
      Vortprovizo::Genpage.genpage(path)
    end

    # helper function to parse a path
    def parse_path_info(path, path_list = [ ])
       path.strip!
       while path.start_with? '/'
         path = path[1..-1]
       end

       path_data = Hash[ path.split('/').each_with_index.map { |a,i| [ path_list[i], a ]  } ]
       if path_data.length != path_list.length
         @warnings = 'Bad'
       end
       path_data
    end

    def initialize(env)
      super
      query_string = env['QUERY_STRING']
      @set_cookies = Array.new

      cookies =
      begin
        if (env['HTTP_COOKIE'])
          HTTP::Cookie.cookie_value_to_hash(env['HTTP_COOKIE'])
        end
      rescue => e
        STDERR.puts 'Cookie Error: ' + e.to_s + ' - ' + env['HTTP_COOKIE'].to_s + e.backtrace.to_json
        ''
      end
      params = CGI::parse(query_string)
      env['params'] = params
      @params = params
      post_data = env['rack.input'].read.force_encoding('utf-8')
      env['post_data'] = post_data
      if (env['CONTENT_TYPE'] || '').match(/json/)
        env['post_json_data'] = JSON.parse(post_data)
      end

      env['post_my_params'] = post_data.split(/[&;]/).inject(Hash.new { |h,k| h[k] = Array.new }) do |hash, k|
        x = k.split(/=/).map { |i| CGI.unescape(i) }
        hash[x[0]].push x[1]
        hash
      end

      env['post_params'] = @post_params = CGI.parse(post_data)

      session = @session = Vortprovizo::Session.new(cookies && cookies['sessionId'] || nil)
      if (session.new_session?)
        set_cookies.push session.get_cookie
      end

      @languages = Vortprovizo::Tools.parse_accept_language(env['HTTP_ACCEPT_LANGUAGE'] || 'en')

  #      @path_params = self.parse_path_info(CGI.unescape(env['PATH_INFO']), 'path_param_names' )
  #
  #      @params = @path_params.merge(@post_params)

  #        @lang = Vortprovizo::Languages.get(lang)
  #        File.open('../html/en/phrases.html', 'r:UTF-8') do |file|
  #          @doc = Nokogiri::HTML5(file)
  #        end
  #        @conn = Vortprovizo::Pg.connection

    end
  end
end
