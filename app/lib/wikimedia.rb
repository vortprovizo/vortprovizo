# frozen_string_literal: true

require 'uri'
require 'net/https'
require 'digest'

# Wikimedia related methods
#
# Mostly having to do with audio files and images
class Wikimedia
  # Given a file name return a URI to the file page
  #
  def self.file_path(file_name)
    file = file_name.dup
    ret = 'https://commons.wikimedia.org/wiki/File:%s' % [ file ]
    if file.match(/.svg$/)
#       ret += '.png'
    elsif file.match(/.tif$/)
#       ret += '.jpg'
    end
    return ret
  end

  # Given a file name return a URI to a thumbnail of the given size
  # @param file_name [String] The file name
  # @param size [Number] the size of thumbname
  def self.thumbnail(file_name, size = 190)
    file = file_name.dup
    file = file.gsub(' ', '_')
    file[0] = file[0].upcase
    size ||= 120
    string = Digest::MD5.hexdigest file
    file = URI.encode_www_form_component(file)

    ret = 'https://upload.wikimedia.org/wikipedia/commons/thumb/%s/%s/%s/%dpx-%s' %
      [ string[0], string[0..1],  file, size, file ]

    if file.match(/\.svg$/)
      ret += '.png'
    elsif file.match(/\.tif$/)
      ret += '.jpg'
    end
    return ret
  end

  # Given a file name return a URL to the wikimedia audio file
  # @param file_name [String] the format type, `:text` or `:html`
  # @return [String] URL to the audio file
  #
  # @example Wikimedia.audio('angry') : {http://upload.wikimedia.org/wikipedia/commons/8/84/LL-Q1860_%28eng%29-Nattes_%C3%A0_chat-angry.wav Angry}
  #     Wikimedia.audio('angry') #=> 'http://upload.wikimedia.org/wikipedia/commons/8/84/LL-Q1860_%28eng%29-Nattes_%C3%A0_chat-angry.wav'
  def self.audio(file_name)
    filename = file_name.gsub(' ', '_')
    filename[0] = filename[0].upcase
    d = Digest::MD5.hexdigest filename
    filename = URI.encode_www_form_component(filename)

    return 'https://upload.wikimedia.org/wikipedia/commons/%s/%s/%s' % [ d[0], d[0..1], filename ]
  end

  # Get the URI of a wikipedia image file
  # @param file_name [String] the base filename
  # @return [String] URL to the audio file
  def self.uri(file_name)
    filename = file_name.gsub(' ', '_')
    filename[0] = filename[0].upcase
    d = Digest::MD5.hexdigest filename
    return URI('https://upload.wikimedia.org/wikipedia/commons/%s/%s/%s' % [ d[0], d[0..1], URI.encode_www_form_component(filename) ])
  end

  # Check if an image file exists
  # @param name [String] the format type, `:text` or `:html`
  # @param size [String] the format type, `:text` or `:html`
  # @return [String] URL to the validated audio file
  # @raise [String] Bad file name or type
  def self.check_image(name, size = nil)
    if (m = name.match(/File:(.*)$/))
      name = m[1]
    end
    if (m = name.match(/Q[0-9]{1,20}$/))
      return :entity
    end

    url = thumbnail(name, size)

    uri = URI(url)

    Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      ret = http.request_head(uri.request_uri)
      raise 'bad file ' + ret.code unless ret.code.to_i == 200
    end

    return name
  end

  # Check if an audio file exists
  # @param name [String] the format type, `:text` or `:html`
  # @return [String] URL to the validated audio file
  # @raise [String] Bad file name or type
  def self.check_audio(name)
    if (m = name.match(/File:(.*)$/))
      name = m[1]
    end
    uri = URI(audio(name))
    Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      ret = http.request_head(uri.request_uri)

      if ret.code.to_i == 404
        return nil
      end

      if (m = ret['content-type'].match(%r{application/(.*)}))
        case m[1]
        when 'ogg'
        when 'wav'
        else
          raise m[1]
        end
      elsif (m = ret['content-type'].match(%r{audio/(.*)}))
        case m[1]
        when 'x-wav'
        else
          raise m[1]
        end
      else
        raise ret['content-type']
      end
      raise 'bad file ' + ret.code unless ret.code.to_i == 200
    end
    return name
  end

  # Is an entity valid
  def self.check_entity(name)
    true
  end

  # Check the type of a file
  # @param name [String] the format type, `:text` or `:html`
  # @return [String] URL to the validated audio file
  # @raise [String] Bad file name or type
  def self.check_type(name)
    if name.match(/^Q[0-9]*$/)
      return :entity
    end
    uri = uri(name)
    ret = nil

    Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      ret = http.request_head(uri.request_uri)

      case ret.code.to_i
      when 200
        case ret['content-type'].to_s
        when 'application/ogg', 'audio/x-wav'
          return :audio
        when 'image/jpeg', 'image/svg+xml', 'image/png', 'image/tiff', 'image/gif'
          return :image
        else
          raise 'Unknown type: %s (%s)' % [ ret['content-type'], uri.to_s ]
        end
      else
        raise 'File not found: %s (%s)' % [ name, uri.to_s ]
      end
    end
    raise 'Unknown error'
  end
end
