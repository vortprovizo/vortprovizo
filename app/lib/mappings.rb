# frozen_string_literal: true

require 'set'

# This class is a [Rack middleware](https://www.rubyguides.com/2018/09/rack-middleware/).
class Mappings
  @@packages = Array[]
  @@mappings = Array[]
  @@mappingset = Set.new

  # This class contains a path to class mapping
  class Mapping
    attr_reader :package
    attr_reader :route
    attr_reader :regex
    attr_reader :canonical

    def initialize(route: nil, regexp: nil, package: nil, canonical: nil)
      @route = route
      @regex = %r{#{sprintf('^%s$' % [ regexp ])}}
      @package = package
      @canonical = canonical
    end

    # Test if the given *path* matched this Mapping
    # @param path [String]
    # @return [MatchData]
    def match(path)
      @regex.match(path)
    end

    # Call the Rack application defined by this Mapping.
    # @param env [Hash] The Rack environment
    # @return [Array|String] The Rack return value
    def call(env)
      ret = @package.call(env)

      return ret
    end

    # to_h
    # @return [Hash]
    def to_h
      return {
        'route' => @route,
        'nroute' => @regex,
        'package' => @package,
      }
    end
  end

  # Register a path to a package
  # @param [#get] package
  # @param [Array<String>] routes
  # @return self
  def self.register(package, routes)
    routes.each do |route|
      nroute = route
      croute = route
      if route.match(/{[a-z]{1,}}/)  # match the keys '{id}'
        nroute = route.gsub(/{([a-z]*)}/, '(?<\1>[^/]*)')
        croute = route.gsub(/{([a-z]*)}/, '{key}')
      end
      if @@mappingset.add?(croute)
        @@mappings.push Mapping.new(canonical: croute, regexp: nroute, package: package, route: route)
      else
        @@mappings.each do |m|
          next if m.canonical != croute
          if package == m.package && route == m.route
            puts "ignoring duplicate definition of route %s in package %s" % [ route, package ]
            next
          end
          puts "The package '%s' redfines the route '%s' already defind in '%s' as '%s'" % [ package, route, m.package, m.route ]
        end
      end
    end
    self
  end

  # Get the list of mappings
  # @return [Array<Mapping>]
  def self.mappings
    @@mappings
  end

  # Get the list of packages
  # @return [Array<Class>]
  def self.packages
    @@packages
  end

  def initialize(app = nil)
    @mappings = @@mappings

    puts "APP: %s %s %s" % [ app, app.to_s, app.inspect ] if ENV['TESTING']

    @app = app
  end

  # Look up the Rack application located in the Mappings or pass on the request
  # @param env [Hash] The Rack environment
  # @return [Array|String]
  def call(env)
    path = env['PATH_INFO']

    @mappings.each do |r|
      if (m = r.match(path))
        queries = env['QUERY_STRING'].split(/[\&;]/)
        queries.push(m.named_captures.map { |k, v| '%s=%s' % [ k, v ] })
        env['QUERY_STRING'] = queries.join('&')

        ret = r.call(env)

        return ret
      end
    end

    @app.call(env)
  end
end
