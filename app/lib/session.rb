# frozen_string_literal: true

# A Rack Middleware Object
#
# This middleware keeps a version of the session in Rack Environment
# This middleware reads or creates a Session Cookie.
#
# @example Use in Config.ru
#  require 'session'
#  use Session
#
class Session
  # @param [Rack Application] app the Rack application
  def initialize(app)
    @app = app
  end

  # Handle *sesion_id* in env['HTTP_COOKIE']['session_id']
  #
  # * Create or extrack `session_id` from the Cookie.
  # * Call the `app`
  # * Update the COOKIE in the app return structure.
  #
  # @param [Rack Environment] env the Rack environment
  # @return [Rack Triple] With session_id in cookie
  def call(env)
    cookie =
      begin
        if env['HTTP_COOKIE']
          puts env['HTTP_COOKIE']
          HTTP::Cookie.cookie_value_to_hash(env['HTTP_COOKIE'])
        end
      rescue StandardError => e
        warn 'Cookie Error: %s - %s %s' % [ e.to_s, env['HTTP_COOKIE'].to_s, e.backtrace.to_json ]
      end

    session = Vortprovizo::Session.new(cookie && cookie['sessionId'] || nil)
    env['session'] = session

    ret = @app.call(env)

    if ret[1].is_a?(Hash)
      if session.new_session?
        ret[1]['Set-Cookie'] = session.get_cookie.set_cookie_value
      end
    else
      puts 'Bad return'
    end

    ret
  end
end
