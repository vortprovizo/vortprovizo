#!/usr/bin/env ruby
# frozen_string_literal: true

$LOAD_PATH.unshift(File.expand_path('lib', File.dirname(__FILE__)))

require 'json'


require 'http'
require 'mappings'
require 'vortprovizo'
require 'vortprovizo/http/web'
require 'session'
require 'logger'

$load = Array[
  'vortprovizo/http/api/demo',
  'vortprovizo/http/api/phrase',
  'vortprovizo/http/api/file',
  'vortprovizo/http/api/oauth2',
  'vortprovizo/http/web/index',
  'vortprovizo/http/web/check',
  'vortprovizo/http/web/phrases',
  'vortprovizo/http/web/phrase',
  'vortprovizo/http/web/images',
  'vortprovizo/http/web/image',
  'vortprovizo/http/web/users/entry',
  'vortprovizo/http/web/users/login',
]

http = Http.new(ENV['PASSENGER_APP_ENV'] || ENV['RACK_ENV'] || 'development')

if http.development?
  use Rack::Reloader
  use Rack::Lint 
  use Rack::ShowExceptions
end

use Rack::Logger
use Session

$load.each do |file|
  begin
    require file
  rescue LoadError => e
    puts "Could not load '%s' %s" % [ file, e ]
    puts e.backtrace
  end
end

if http.development?
  map '/assets' do
    run Rack::File.new('../html/assets')
  end

  map '/favicon.ico' do
    data = File.read('../html/favicon.ico')
    run ->(_env) { [ 200, { 'content-type' => 'image/vnd.microsoft.icon' }, [ data ] ] }
  end

  map '/html' do
    run Rack::File.new('../html')
  end

  map '/images/image.png' do
    run Rack::File.new('../html')
  end

  map '/dev/start-up' do
    run ->(_env) { return [ 200, { 'content-type' => 'application/json; charset=utf-8' }, [ ENV.to_h.to_json ] ] }
  end

  map '/dev/env' do
    run ->(env) { return [ 200, { 'content-type' => 'application/json; charset=utf-8' }, [ env.to_json ] ] }
  end

  map '/dev/http' do
    run ->(_env) { return [ 200, { 'content-type' => 'application/json; charset=utf-8' }, [ 'c', http.to_json ] ] }
  end
  map '/dev/routes' do
    run ->(_env) {
      [ 200, { 'content-type' => 'application/json; charset=utf-8' }, [ Mappings.mappings.map { |x| x.to_h }.to_json ] ]
    }
  end
end

use Mappings

run http

# eof
