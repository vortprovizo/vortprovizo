# frozen_string_literal: true

require 'pg'
require 'wikimedia'

require 'pp'

connection = PG.connect dbname: 'vortprovizo'

connection.prepare('get_audio', <<~SQL)
  select p.phrase
       , p.id as phrase_id
       , p.language_id
       , a.file
    from phrases p
    left join phrase_audio pa
      on pa.phrase_id = p.id
    left join audio a
      on pa.audio_id = a.id
   where p.language_id = $1
SQL

connection.exec_prepared('get_audio', [ 1 ]).each do |row|
  phrase = row['phrase']
  # phrase_id = row['phrase_id']
  # language_id = row['language_id']
  if (m = phrase.match(/^(?:het|de) (.*)$/))
    phrase = m[1]
  end
  phrase = 'Nl-' + phrase
  file = nil
  [ 'wav', 'ogg' ].each do |suf|
    temp = phrase + '.' + suf
    if Wikimedia.check_audio(temp)
      file = temp
    end
  end
  if row['file'] && !file
    puts 'Remove File %s' % [ row['file'] ]
    next
  end
  if row['file']
    print "File %s %s\n" % [ row['file'], file || 'na' ]
    next
  end
  p row
end

__END__
  next
  if file
    begin
      rd = connection.exec(<<~SQL, [ file, language_id ])
      INSERT INTO audio (file, language_id) values ($1, $2)
      SQL
      rd = connection.exec(%q|SELECT currval(pg_get_serial_sequence('audio','id')) as audio_id|);
    audio_id = rd[0]['audio_id']
      rd = connection.exec(<<~SQL, [ phrase_id, audio_id ])
      INSERT INTO phrase_audio (phrase_id, audio_id) values ($1, $2)
      SQL
    rescue => e
      pp e
    end
  end

