# Vortprovizo

https://nokogiri.org/tutorials/modifying_an_html_xml_document.html

* https://developer.mozilla.org/en-US/docs/Web/Media/Autoplay_guide

```
sudo apt-get install ruby-narray
```

```
yard stats --list-undoc
```

```
DELETE FROM
  tableA
WHERE
  id NOT IN(
    SELECT
        id
    FROM
        tableA
    WHERE
        username = 'abcdefg'
    ORDER BY createdat ASC LIMIT 20)
  AND
    username = 'abcdefg'
```


  id  | phrase_id | image_id
------+-----------+----------
 3994 |      1058 |    10094
 4005 |      2071 |    10105
 4006 |      1237 |    10105


update audio set file = REGEXP_REPLACE(file, ' ', '_', 'g')  where file ~ ' ';
update images set file = REGEXP_REPLACE(file, ' ', '_', 'g')  where file ~ ' ';

alter table audio add constraint file check (file !~ ' ');
alter table images add constraint file check (file !~ ' ');


### Manifest 

* https://developer.mozilla.org/en-US/docs/Web/Manifest/icons

